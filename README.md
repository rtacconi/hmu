== Welcome to Hitmeup

Run the app locally with this URL: lvh.me:3000 (no need to setup lvh.me in your /etc/hosts file)
	
You can push your changes to staging branch on Github with:

git push origin staging

and to master with:

git push origin master

To deploy to staging (you need riccardo.pem in ~/.ssh):

bundle exec staging deploy

To deploy to production (you need riccardo.pem in ~/.ssh):

bundle exec production deploy

To see the app logs

ssh to EC2 instance then:

cd staging/current
tail -f log/staging.log

You can use rake db:seed to get some initial data for development or you can download a backup from xeround's phpmyadmin. You should use MySql in development and production.

A HTML report of RSpec can be found here at spec/rspec_report.html and can be re-generated with:

bundle exec rake rspec_report

A HTML report of User Acceptance Testing can be found at spec/requests_report.html and can be re-generated with:

bundle exec rake requests_report

S3 storage
Access: AKIAJ64O7WFSPOLJ3PGA
Secret: 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

== Paypal pyament options:

Delay chain, refound if deal is not completed
Pre-approval
We can have primary and secondary receiver
This is an adapter for ActiveMerchant: https://github.com/lamp/paypal_adaptive_gateway
This is a gem for paypal adaptive: https://github.com/tc/paypal_adaptive
Tutorial: http://webtempest.com/paypal-adaptive-with-rails-3/

== Facebook

http://lvh.me:3000/facebook_callback/2?code=AQAawySioJ4E0kUFBtYOTPRyXSsgX9P-zWW8EATzOdTzghpozzUMAmaIczXe8WAsA0601i94sN8CRvo-XHonRxEmuYHxbShQ94ybgrrvweRBMPZBbLbDEi9_BafJp77vcVJCvWytkpG-QNGbM3NHalu4SywCABYQPGlRMDYUHEoH2lOvvQ7bJFd7SzjpeduNNoE#_=_

{"code"=>"AQAawySioJ4E0kUFBtYOTPRyXSsgX9P-zWW8EATzOdTzghpozzUMAmaIczXe8WAsA0601i94sN8CRvo-XHonRxEmuYHxbShQ94ybgrrvweRBMPZBbLbDEi9_BafJp77vcVJCvWytkpG-QNGbM3NHalu4SywCABYQPGlRMDYUHEoH2lOvvQ7bJFd7SzjpeduNNoE",
 "id"=>"2"}
 
== Admin area
To restrict access to non-admin users - in a controller - you just need to inherit from Admin::AdminController. Es.:

class Admin::AnalyticsController < Admin::AdminController

== CSS, JS and assets pipeline
You have to declare you CSS or JS file in application.js or applicaiton.css otherwise they will not be included in staging/production and you will create bugs!

== Process monitoring
Bluepill gem monitors the delayed_job worker but it could be used to monitor other processes (i.e. Apache).
whenever gem starts bluepill process at every reboot and during deployment bluepill is restarted.

bluepill conf file is at: config/RAILS_ENV.pill
whenever conf file is at: config/schedule.rb

== Spreedly Core

SAGEPAY

curl -u 'C8Cec2mhFBW954Xk60ws1d1VF7m:r7HE4i8HqhmFEC4MEOrfZ8WmwFlnW19sK9hLk91elcTlBrSfo5w1TrNuWn7ulrwL' \
    -H 'Content-Type: application/xml' \
    -d '<gateway>
          <gateway_type>sage_pay</gateway_type>
          <login>hitmeupltd1</login>
        </gateway>' \
    https://spreedlycore.com/v1/gateways.xml

    <gateway>
      <token>Uev9LcHFX4q59ZjMbgeFp7nOZqS</token>
      <gateway_type>sage_pay</gateway_type>
      <login>hitmeupltd1</login>
      <characteristics>
        <supports_purchase type="boolean">true</supports_purchase>
        <supports_authorize type="boolean">true</supports_authorize>
        <supports_capture type="boolean">true</supports_capture>
        <supports_credit type="boolean">true</supports_credit>
        <supports_void type="boolean">true</supports_void>
        <supports_reference_purchase type="boolean">false</supports_reference_purchase>
      </characteristics>
      <redacted type="boolean">false</redacted>
      <created_at type="datetime">2012-04-17T16:22:52Z</created_at>
      <updated_at type="datetime">2012-04-17T16:22:52Z</updated_at>
    </gateway>



TESTING

<gateway>
  <token>8sBCm1gMt3Sz394Mmu2KIAANlBy</token>
  <gateway_type>test</gateway_type>
  <characteristics>
    <supports_purchase type="boolean">true</supports_purchase>
    <supports_authorize type="boolean">true</supports_authorize>
    <supports_capture type="boolean">true</supports_capture>
    <supports_credit type="boolean">true</supports_credit>
    <supports_void type="boolean">true</supports_void>
  </characteristics>
  <redacted type="boolean">false</redacted>
  <created_at type="datetime">2012-03-22T15:13:27Z</created_at>
  <updated_at type="datetime">2012-03-22T15:13:27Z</updated_at>
</gateway>


curl -u 'C8Cec2mhFBW954Xk60ws1d1VF7m:r7HE4i8HqhmFEC4MEOrfZ8WmwFlnW19sK9hLk91elcTlBrSfo5w1TrNuWn7ulrwL' \
  -H 'Content-Type: application/xml' \
  -d '<transaction>
    <transaction_type>authorize</transaction_type>
    <payment_method_token>Hy7wSgx5jLxVvwVAwIfoRyaEUsC</payment_method_token>
    <amount>100</amount>
    <currency_code>USD</currency_code>
  </transaction>' \
  https://spreedlycore.com/v1/gateways/8sBCm1gMt3Sz394Mmu2KIAANlBy/authorize.xml
  
curl -u 'C8Cec2mhFBW954Xk60ws1d1VF7m:r7HE4i8HqhmFEC4MEOrfZ8WmwFlnW19sK9hLk91elcTlBrSfo5w1TrNuWn7ulrwL' \
    -H 'Content-Type: application/xml' \
    -d '' \
    https://spreedlycore.com/v1/transactions/8sBCm1gMt3Sz394Mmu2KIAANlBy/capture.xml
    
    
    Card        Type	Good Card   Failed Card
    Visa	      411111111111111   4012888888881881
    MasterCard	5555555555554444	5105105105105100
    Amer. Ex. 	378282246310005	  371449635398431
    Discover	  6011111111111117	6011000990139424



curl -u 'C8Cec2mhFBW954Xk60ws1d1VF7m:r7HE4i8HqhmFEC4MEOrfZ8WmwFlnW19sK9hLk91elcTlBrSfo5w1TrNuWn7ulrwL' \
     -H 'Content-Type: application/xml' \
     -d '' \
     -v \
     https://spreedlycore.com/v1/transactions/CBQevitvuZArLEIDKdSD1eO8ZZM/capture.xml