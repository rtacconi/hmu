// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
// do not require mercury.js here
//
//= require jquery
//= require twitter/bootstrap
//= require jquery_ujs
//= require jquery-ui.custom.min.js
//= require jquery.minicolors.min.js
//= require parameters.js
//= require timepicker.js
//= require pages.js
//= require deals.js
//= require max_length_plugin.js
//= require company.js
//= require dropdown.js
//= require textarea_word_count.js
//= require highcharts.js
//= require flash_message.js
//= require popup_checkout.js
//= require countdown.js
//= require disable.js