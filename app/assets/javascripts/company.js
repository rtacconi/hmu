$(document).ready(function() {  

  
  // open previews in new window
  $("a img, input.btn").each(function(){
    $.data(this, 'clicked', false);
  }).click(function(){
   // some standard behaviours for buttons
   
   // prevent clicking twice
   if ($.data(this, 'clicked')) {
     e.preventDefault();
   }
   
   // set to clicked
   $.data(this, 'clicked', true);
   
   // fade them on hover   
   $(this).unbind().fadeTo(300, 0.2);    
   
  }).hover(
 	  function(){ $(this).fadeTo(300, 0.8); }, 
 		function(){ 
 		  if (!$.data(this, 'clicked')) {
   		  $(this).fadeTo(300, 1); 		    
 		  }
     }
  );
  

   
});
