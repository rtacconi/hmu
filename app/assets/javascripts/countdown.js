var countdown_is_running = true;
var timer;
var seconds;

var seconds_in_a_minute = 60;
var seconds_in_an_hour = 3600;
var seconds_in_a_day = seconds_in_an_hour * 24;

 

$(document).ready(function() {  
  timer = $(".timer");
  if (timer.length) {
    
    seconds = parseInt($(".seconds", timer).html());
    seconds += parseInt($(".minutes", timer).html()) * seconds_in_a_minute;
    seconds += parseInt($(".hours", timer).html()) * seconds_in_an_hour;
    seconds += parseInt($(".days", timer).html()) * seconds_in_a_day;
    // we have a time on page, so set off counting down a second at a time
    update_countdown();
    setInterval("update_countdown()",1000);
    
    // fade in our fields
    $(".row", timer).css('visibility', 'visible');
  }
  

   
});


function update_countdown(){
  if (countdown_is_running) { 
    one_second_gone();
    update_displays();
  } else {
    countdown_is_complete();
  }
}

function one_second_gone() {
  seconds -= 1;
  countdown_is_running = (seconds > 0);
}

function update_displays(){     
  var seconds_temp = seconds;
  var days = 0;
  var hours = 0;
  var minutes = 0; 
  var finished = false;
  
  while (!finished) {
    if (seconds_temp >= seconds_in_a_day) {
      days += 1; 
      seconds_temp -= seconds_in_a_day;
    } else if (seconds_temp >= seconds_in_an_hour) {
      hours += 1; 
      seconds_temp -= seconds_in_an_hour;
    } else if (seconds_temp >= seconds_in_a_minute) {
      minutes += 1; 
      seconds_temp -= seconds_in_a_minute;
    } else {
      finished = true;
    }
  }
  
  update_display($(".seconds", timer), seconds_temp, true, (minutes == 0 && hours == 0 && days == 0));
  update_display($(".minutes", timer), minutes, true, (hours == 0 && days == 0));
  update_display($(".hours", timer), hours, true, (days == 0));
  update_display($(".days", timer), days, false, true);
}

function update_display(field, value, with_leading_zeros, turn_red_if_zero) {
  // turn red if value zero
  field.css('color', ((turn_red_if_zero && value == 0) ? '#930310' : '#151515'));
  if (with_leading_zeros) {
    // make in to string
    value = value + "";
    // make sure it's 2 chars long
    while (value.length < 2) {
      value = "0" + value;
    }
  }
  field.html(value);
}

function countdown_is_complete(){
  // fade out the times and then reload the page, which should be update to reflect the new status
  // since the deal has now either started or ended, so the options will be different
  $(".row", timer).fadeOut(1000, function(){
    location.reload();
  })
}