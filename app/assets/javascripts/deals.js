$('document').ready(function() {
  $('#deal_start_time').datetimepicker({dateFormat: dateApplicationFormat});
  $('#deal_end_time').datetimepicker({dateFormat: dateApplicationFormat});
  $('#deal_voucher_expiry').datetimepicker({dateFormat: dateApplicationFormat});
  $('#deal_photo').on('change', function() { 
    $("#preview").html('');
    $("#preview").html('<img src="/assets/loader.gif" alt="Uploading..."/>');
    var formData = new FormData($('form')[0]);
    $.ajax({
        url: '/image',  //server script to process data
        type: 'POST',
        // Form data
        data: formData,
        //Options to tell JQuery not to process data or worry about content-type
        cache: false,
        contentType: false,
        dataType: 'script',
        processData: false,
        // events
        success: function(data) {
          script = $(data).text();
          eval(script);
        }
    });
  });
  
  $('input[name*="commit"]').click(function() {
    $('#deal_photo').remove();
  });
  
  $('#deal_expiry_time').datetimepicker({dateFormat: dateApplicationFormat});
  
  if ($('.edit_deal').length > 0) {
    deal_id = $('.edit_deal').attr('id').replace (/[^\d]/g, "");
    $.ajax({
        url: '/image/deal_picture/' + deal_id,  //server script to process data
        type: 'GET',
        //Options to tell JQuery not to process data or worry about content-type
        cache: false,
        contentType: false,
        dataType: 'script',
        processData: false,
        // events
        success: function(data) {
          script = $(data).text();
          eval(script);
        }
    });
  }
});