$('document').ready(function() {
  $('#my-account').mouseover(function() {
    $('.drop').show();
  });

  $('#my-account').mouseleave(function() {
    $('.drop').hide();
  });

  $('.drop').mouseover(function() {
    $('.drop').show();
  });

  $('.drop').mouseleave(function() {
    $('.drop').hide();
  });
});