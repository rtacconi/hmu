$(document).ready(function() {
  $(document).ajaxSend(function(e, xhr, options) {
    var token = $("meta[name='csrf-token']").attr("content");
    xhr.setRequestHeader("X-CSRF-Token", token);
  });
  
  $('#page_published').change(function(e) {
    var pathname = window.location.pathname;
    
    if($('#page_published').prop("checked")) {
      $.ajax({
        url: pathname + '.json',
        type: 'PUT',
        data: { _method:'PUT', page : { published: '1' } },
        dataType: 'json',
        context: document.body
      });
      alert('Page published');
    } else {
      $.ajax({
        url: pathname + '.json',
        type: 'PUT',
        data: { _method:'PUT', page : { published: '0' } },
        dataType: 'json',
        context: document.body
      });
      alert('Page not published');
    }
  });
  
  $('.change-section').change(function(e) {
    var id = e.target.id
    element = $('#' + id);
    var section_id = element.val();
    alert('page_id: '+id+' section_id: '+section_id);
    $.ajax({
      url:  window.location.pathname + '.json',
      type: 'PUT',
      data: { _method:'PUT', page : { section_id: section_id } },
      dataType: 'json',
      context: document.body
    });
  });
});