$(document).ready(function() {  

  
  // open previews in new window
  $("a.pop-up-checkout").unbind("click").each(function(){
    // override the live link behaviour so that we open in a new window and leave the current page as is
    $(this).click(function(e){
      e.preventDefault();
      window.open($(this).attr('href'),'','fullscreen=no,location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,width=800,height=700');
    });
  }); 
  
  $("a.pop-up-facebook").unbind("click").each(function(){
    // override the live link behaviour so that we open in a new window and leave the current page as is
    $(this).click(function(e){
      e.preventDefault();
      window.open($(this).attr('href'),'','fullscreen=no,location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,width=1010,height=600');
      if($(this).hasClass("and-remove")) {
        $(this).fadeOut(500);
      }
    });
  });
  
  $("a.close-window").each(function(){
    $(this).click(function(e){
      e.preventDefault();
      self.close ();
    });
  });
  
  
  

   
});
 