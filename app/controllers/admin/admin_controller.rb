class Admin::AdminController < ApplicationController
  before_filter :require_admin
  before_filter :set_title
  
  protected
  
    def set_title
      @title = "Admin Dashboard"
    end
end
