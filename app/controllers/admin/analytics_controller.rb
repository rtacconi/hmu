class Admin::AnalyticsController < Admin::AdminController
    
  # GET /admin/analytics
  # GET /admin/analytics.json
  def index
    # go back to when first user was created
    @periods = generate_reporting_periods(User.first.created_at)
    @data = Hash.new
    @periods.each do |period|
      @data[period[:start]] = Payment.revenue(period[:start], period[:end])
    end
  end

end
