class Admin::LogsController < Admin::AdminController
  
  def index
    if params[:search].present?
      @logs = DLog.find_with_index(params[:search])
    else
      @only_errors_and_warnings = params[:only_warnings_and_errors] == "yes"
      @only_errors = params[:only_errors] == "yes"
      if @only_errors_and_warnings
        logs = DLog.warnings_and_errors.order('id desc')
      elsif @only_errors
        logs = DLog.errors.order('id desc')
      else
        logs = DLog.order('id desc')
      end
      @logs = logs.page(params[:page]).per(50)
    
      @title = "Logs"
      @subtitle = if @only_errors_and_warnings 
        "Errors & Warnings Only"
      elsif @only_errors
        "Errors Only"
      else
        "All"
      end
    end
  end
  
  def show
    @log = DLog.find(params[:id])
    @title = "Admin Logs"
    @subtitle = "Entry #{@log.id}"
  end
  
end
