class Admin::MonitorController < Admin::AdminController
  def index
    @title = "System monitor"
    
    begin
      unless Rails.env == 'test'
        @bluepill = %x[cd #{Rails.root} && sudo bluepill status]
      else
        @bluepill = ''
      end
      
      @dj = %x[cd #{Rails.root} && RAILS_ENV=#{Rails.root} script/delayed_job status]
    rescue
      @bluepill = 'I could not run the command because an error happened'
    end
  end

end
