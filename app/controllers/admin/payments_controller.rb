class Admin::PaymentsController < Admin::AdminController
  def index
    @title = "Payments"
    if params[:deal_id]
      @deal = Deal.find(params[:deal_id])
      @payments = @deal.payments
      @subtitle = "For #{@deal}"
    else
      @payments = Payment
      @subtitle = "All"
    end
    @payments = @payments.order('transaction_date desc').page(params[:page]).per(50)
  end
  
  def refund
    @payment = Payment.find(params[:id])
    @title = "Payment #{@payment.id}"
    @subtitle = "Refund"
    
    if request.post?
      if @payment.refund!
        redirect_to payments_deal_path(@payment.deal), notice: "Payment ID ##{@payment.id} refunded"
      else
        redirect_to payments_deal_path(@payment.deal), alert: "Failed to refund payment ##{@payment.id}"
      end
    end
  end
end
