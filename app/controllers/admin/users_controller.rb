class Admin::UsersController < Admin::AdminController
  def index
    @users = User.order('current_sign_in_at desc').page(params[:page]).per(50)
  end
end
