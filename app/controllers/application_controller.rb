class ApplicationController < ActionController::Base
  protect_from_forgery
  
  helper_method :sort_column, :sort_direction
  
  before_filter :check_completed

  def log_and_handle(e)  
    # ignore active record not found errors
    if e.class == ActiveRecord::RecordNotFound
      render not_found_pages_path
    else
      DLog.error "Application error encountered #{e.message} at #{e.backtrace}", :request => request
      # redirect_to(:controller => 'errors', :action => (ActionController::RoutingError === e  ? "page_not_found" : "sorry" ))
      render whoops_pages_path
    end
  end
    
  # catch all application problems
  rescue_from Exception, :with => :log_and_handle unless Rails.env.development?
  

  
  def require_admin
    unless current_user && current_user.admin?
      flash[:alert] = 'Permission denied'
      redirect_to root_path
    end
  end  
  
  def set_always_show_header
    @show_custom_seller_header = false
  end 
  
  def delayed_job_admin_authentication
    require_admin
  end

  # why here? This should stay in the models
  def generate_reporting_periods(start_date, granularity = :weeks, end_date = Time.zone.now.to_date)
    # figure out where we're reporting from, how far back
    # start_date = params[:period] != "all" ? params[:period].to_i.months.ago : user.account.created_at
    # granularity = params[:granularity]
    
    # first let's get all the reporting periods together
    granuality_is_months = granularity == :months
    granuality_is_weeks = granularity == :weeks
    granuality_is_days = granularity == :days

    if granuality_is_days
      period_length = 1.day
    elsif granuality_is_weeks
      period_length = 1.week
      # round back to start of week
      until start_date.strftime("%a") == "Mon"
        # RLog.info self, "Date is #{end_date}"
        start_date = start_date - 1.day
      end
    else
      period_length = 1.month
      # round back to start of month
      until start_date.strftime("%d") == "01"
        start_date = start_date - 1.day
      end
    end    
    
    @periods = Array.new      
    # now break in to reporting periods
    until start_date > end_date
      period_start = start_date
      start_date = start_date + period_length
      # then add the period end based on the new start minus one day
      period_end = start_date - 1.day
      @periods << {:start => period_start, :end => period_end}
    end
    
    # limit output so we don't kill the server
    @periods[0..31]
  end   
  
  protected
    
    def fb_callback_url(id)
      "http://#{request.host_with_port}/facebook_callback/#{id}"
    end
    
    def require_ssl 
      # get rid of subdomains so we don't get ssl errors
      redirect_without_subdomain_if_subdomain
      # unless Rails.env == 'development'
      if Rails.env.production?        
        redirect_to :protocol => "https://" unless (request.ssl?)
      end
    end
    
    def redirect_without_subdomain_if_subdomain
      if request.subdomain.present? && Rails.env.production? 
        redirect_to "http://#{APP_CONFIG['SITE']}#{request.fullpath}"
      end
    end
    
    def sort_column
      # Product.column_names.include?(params[:sort]) ? params[:sort] : "name"
      params[:sort] ? params[:sort] : "id"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end
    

    def check_completed
      if current_user
        return if current_user.admin?
        return if current_user.company.confirmation_state == "design_completed"
        return if params[:controller] == 'company'
        
        if current_user.company.confirmation_state == "design_completed"
          # redirect_to settings_path and return unless params[:controller] == 'settings'
        end

        if current_user.company.confirmation_state == "profile_completed"
          redirect_to design_path and return unless params[:controller] == 'design'
        end

        if current_user.company.confirmation_state == "not_completed"
          unless params[:controller] == 'profile'
            redirect_to profile_path and return
          end
        end
      end
    end
end