class BlogController < ApplicationController 
  before_filter :is_blog
  before_filter :set_titles
  
  def index
    @posts = Page.find_posts.published
  end
  
  def show
    @page = Page.find(params[:id])
    render 'pages/show'
  end
  
  
  protected
  
    def is_blog
      @is_blog = true
    end
    
    def set_titles
      @title = "The HitMeUp Blog"
      @intro = "Blog subtitle goes here"
    end
  

end
