class CheckoutController < ApplicationController
  before_filter :require_ssl
  before_filter :check_vouchers_available, :only => [:show]
  layout 'checkout'  
  
  def test
    @payment = Payment.find(75)
    @deal = @payment.deal
    # Notifier.order_confirmation(@payment).deliver
    Notifier.voucher(@payment.voucher_code).deliver
    return render('confirm')
  end
  
  def show
    @credit_card = CreditCard.new
    if params[:payment_id].nil?
      @payment = Payment.new
      @countries = Country.all
      return render('customer_details')
    else
      @payment = Payment.find(params[:payment_id])
      DLog.info "Capturing card details #{@payment.inspect} ", {:payment => @payment}
      return render('card')
    end
  end
  
  def confirm
    return if error_saving_card

    @payment = Payment.find(params[:id])
    
    raise ActionController::RoutingError.new('Payment already authenticated') if @payment.authenticated? 
    
    @deal = Deal.find(@payment.deal_id)
    DLog.info "Payment details captured, params are #{params.inspect} ", {:payment => @payment, :deal => @deal}
    @payment_method_token = params[:token]


    if Rails.env.development?
      payment_capture_successful = true
      response_token = "DEVELOPMENT"
    else     
      @credit_card = CreditCard.new(SpreedlyCore.get_payment_method(@payment_method_token))
      unless @credit_card.valid?
        flash[:alert] = 'Your credit card details are invalid.'        
        DLog.info "Spreedly says these details are invalid #{@credit_card.inspect} ", {:payment => @payment, :deal => @deal}      
        return render('card')
      end   
      
      # need to times price by 100 to convert from pounds to pence e.g. 2.00 becomes 200p
      response = SpreedlyCore.authorize(params[:token], @deal.price * 100)
      payment_capture_successful = response.code == 200
      response_token = response["transaction"]['token']
    end
    
    if payment_capture_successful
      DLog.info "Payment authorised for #{@payment.inspect} ", {:payment => @payment, :deal => @deal}      
      @payment.update_attributes(:token => response_token,
                                 :transaction_date => Time.now)
      DLog.info "Order confirmation sent ", {:payment => @payment, :deal => @deal}      
      
      @payment.update_attribute(:order_confirmation_sent, Time.now)
      Notifier.order_confirmation(@payment).deliver
      return render('confirm')
    end

    DLog.info "Payment failed for #{@payment.inspect} with response #{response.inspect}", {:payment => @payment, :deal => @deal}      
    set_flash_error(response)
    return render('card')
  end 
  
  protected
  
    def check_vouchers_available
      @deal = Deal.find(params[:id])
      if @deal.all_vouchers_purchased?
        flash[:alert] = "Sorry - all vouchers for this deal have now been purchased"
        redirect_to deal_path(@deal)
      end
    end
  
  private
    def set_flash_error(response)
      if response["errors"]
        flash[:alert] = response["errors"].values.first
      else
        flash[:alert] = "#{response['transaction']['response']['message']} #{response['transaction']['response']['error_detail']}" if response['transaction']['response']
      end
    end

    def error_saving_card
      return false if params[:error].blank?

      @credit_card = CreditCard.new
      @error = params[:error]
      render(:action => :buy_tshirt)
      true
    end
end
