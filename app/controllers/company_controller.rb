class CompanyController < ApplicationController 
  
  before_filter :require_admin, :only => [:show, :index, :masquerade, :settle]
  before_filter :set_title, :except => [:index, :update]
  
  # PUT /companies/1
  # PUT /companies/1.json
  def update
    @company = scoper
    
    respond_to do |format|
      if @company.update_attributes(params[:company])
        format.html do
          redirect_to dashboard_path, notice: 'Your profile was successfully updated.'
        end
        
        format.json { head :ok }
      else
        format.html do
          flash[:alert] = 'Your profile was not updated.'
          render '/profile/index'
        end
        
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end          
  
  def show
    @company = scoper  
    @deals = @company.deals
    @user = @company.user
  end
  
  def masquerade
    @company = scoper         
    sign_in(:user, @company.user) 
    DLog.info session.inspect
    redirect_to dashboard_path
  end
  

  def index
    @title = "Admin Dashboard"
    @subtitle = "All Sellers"
    if params[:only_with_outstanding] == "yes"
      @companies = Company.with_outstanding_revenue
    else
      @companies = Company.order(:id).page(params[:page]).per(50)      
    end
  end
  
  def seller_payments
    @company = scoper    
    @subtitle = "Seller Payments"    
    @payments = @company.seller_payments
  end
  
  def settle
    @company = scoper
    @amount = params[:amount]
    @subtitle = "Payment"
    @seller_payment = SellerPayment.new(
      :amount => @amount,
      :user => current_user # record this was made by admin user
      )
    
    if request.post?
      @seller_payment.company = @company
      @seller_payment.bank_name = params[:bank_name]
      @seller_payment.branch = params[:branch]
      @seller_payment.sort_code = params[:sort_code]
      @seller_payment.account_number = params[:account_number]
      if @seller_payment.save
        redirect_to company_index_path(:only_with_outstanding => "yes"), notice: "Seller Payment ID ##{@seller_payment.id} created"
      else
        redirect_to company_index_path(:only_with_outstanding => "yes"), alert: "Failed to create payment"
      end
    else

    end
  end
  
  
  

  
  protected
    def scoper
      current_user && current_user.admin? ? Company.find(params[:id]) : current_user.company
    end
    
    def set_title
      @title = scoper if scoper
    end
    
end
