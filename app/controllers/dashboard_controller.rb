class DashboardController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @company = current_user.company
    @past_deals = Deal.past(@company)
    @current_deals = Deal.current(@company)
    @title = @company
    @subtitle = "Your Dashboard"
    @intro = "This is your hub within HitMeUp - providing you with an overview of your current deals, past deals and company profile"
  end
  
  def revenue  
    @company = current_user.company
    @title = @company
    @subtitle = "Your Revenue"
    @intro = "See the revenue from your deals."
    @deals = @company.deals
    
    @periods = generate_reporting_periods(@company.deals.order("created_at").first.created_at, :weeks)
    
    @weekly_revenue = Hash.new
    @periods.each do |period|
      @weekly_revenue[period[:start]] = @company.revenue_after_commission(period[:start], period[:end])
    end
    
  end


end
