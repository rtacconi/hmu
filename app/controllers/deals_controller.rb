class DealsController < ApplicationController
  before_filter :require_admin, :only => [:index, :payments, :logs]
  before_filter :authenticate_user!, 
                :only => [:my_deals, :publish, :usage, :new, :edit, :create, :update, :destroy, :vouchers]
  before_filter :get_deal_and_company_and_check_permissions
  before_filter :get_deal_and_company_for_public, :only => [:show, :banner, :iframe, :example_iframe, :facebook]
  before_filter :page_has_map, :only => [:show, :new, :edit, :banner, :iframe, :example_iframe, :facebook]  
  before_filter :set_title
  
  # no longer clean fb session
  # after_filter :clean_fb_session
  
  # GET /deals
  # GET /deals.json
  def index  
    @title = "Admin Dashboard"
    @subtitle = "All Deals"
    @deals = Deal.search(params[:search]).order(sort_column + " " + sort_direction).limit(50)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @deals }
    end
  end
  
  def payments
    redirect_to admin_payments_path(:deal_id => @deal.id)
  end
  
  def log
    @only_errors_and_warnings = params[:only_warnings_and_errors] == "yes"
    logs = @only_errors_and_warnings ? DLog.warnings_and_errors.order('id desc') : DLog.order('id desc')
    @logs = logs.where("deal_id = ?", @deal.id).page(params[:page]).per(50)
    @subtitle = "Log"
  end
  
  
  # GET /my_deals
  def my_deals
    @company = current_user.company
    @past_deals = Deal.past(@company)
    @current_deals = Deal.current(@company)
    @title = @company
    @subtitle = "My Deals"
    @intro = "Your deals - past, present and future"
  end

  # GET /deals/1
  # GET /deals/1.json
  def show 
    @title = @deal.company
    @subtitle = "Your Deals"
    @intro = "Your deals - past, present and future."
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @deal }
      return
    end  
  end
  
  def publish
    @subtitle = "Publish"
    @intro = "Publish your deal to your website and facebook page."
    redirect_without_subdomain_if_subdomain
    @oauth = Koala::Facebook::OAuth.new(
      APP_CONFIG['FBAPPID'], 
      APP_CONFIG['FBAPPSECRET'], fb_callback_url(@deal.id)
    )
    # begin
      if session[:facebook]
        @graph = Koala::Facebook::GraphAPI.new(current_user.access_token)
        local_cached_fb_page = FacebookPage.find_by_company_id(current_user.company_id)
        if local_cached_fb_page
          @fb_page = @graph.get_object(local_cached_fb_page.fb_page_id)

          # we need to check if they still have the app installed, if not, delete the fb_page we thought we had
          if !@fb_page["has_added_app"]
            DLog.debug "User is signed in on facebook but they don't have the app installed anymore, deleting page", {:user => current_user}
            local_cached_fb_page.destroy
            @fb_page = nil
          end 
        end

        DLog.debug "User is signed in on facebook, with page #{local_cached_fb_page.inspect} and graph page object #{@fb_page.inspect}", {:user => current_user}
      end
    # rescue
    #   @fb_page = nil
    # end  
  end
  
  def banner
    render :layout => 'banner'
  end
  
  def iframe
    @iframe = true
    render :show, :layout => 'iframe'
  end
  
  def facebook
    @iframe = true
    render :facebook, :layout => 'facebook'
  end
  
  def example_iframe 
    @iframe = true
    render :layout => 'iframe'
  end
  
  def usage
    @subtitle = "Usage"
    @intro = "See the revenue this deal has generated and the number of voucher redemptions."
    @periods = generate_reporting_periods(@deal.start_time, :days, @deal.end_time)
    @data = Hash.new
    @periods.each do |period|
      @data[period[:start]] = @deal.revenue_after_commission(period[:start], period[:end])
    end
  end

  # GET /deals/new
  # GET /deals/new.json
  def new
    @deal = Deal.new(start_time: 2.hours.since, end_time: 3.days.since, link_code: rand(36**8).to_s(36))
    @company = current_user.company
    
    @title = @company
    @subtitle = "Create deal"
    @intro = "Fill in the form below to create a deal in a couple of minutes."

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @deal }
    end
  end

  # GET /deals/1/edit
  def edit
    @subtitle = "Edit"
    @intro = "You can edit this deal until it goes live."
    
    if !@deal.is_editable? && !current_user.admin?
      redirect_to deal_path(@deal), alert: "You can't edit an active deal."
    end
  end

  # POST /deals
  # POST /deals.json
  def create
    @company = current_user.company
    params[:deal].delete(:photo)
    @deal = Deal.new(params[:deal])
    @deal.start_time = DateTime.parse(params[:deal][:start_time]) if params[:deal][:start_time].present?
    @deal.end_time = DateTime.parse(params[:deal][:end_time]) if params[:deal][:end_time].present?
    @deal.company = current_user.company
    
    respond_to do |format|
      if @deal.save 
        flash[:info] = "You may edit your deal until it goes live."
        format.html { redirect_to deal_path(@deal), notice: 'Deal was successfully created.' }
        format.json { render json: @deal, status: :created, location: @deal }
      else
        format.html { render action: "new" }
        format.json { render json: @deal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /deals/1
  # PUT /deals/1.json
  def update
    respond_to do |format|
      format.js do
        # render :js => "alert('Deal was successfully updated.');"
        @deal.attributes(params[:deal])
        @deal.save!(validate: false)
        render :text => image_tag(@deal.photo.url(:large), :class => "preview")
        return
      end
      if @deal.update_attributes(params[:deal])
        #raise scoper.find(params[:id]).inspect 
        flash[:info] = "You may edit your deal until it goes live." if !@deal.started?
        format.html { redirect_to @deal, notice: 'Deal was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @deal.errors, status: :unprocessable_entity }
        format.js { render :js => "alert('Deal not was updated.');" }
      end
    end
  end

  # DELETE /deals/1
  # DELETE /deals/1.json
  def destroy
    @deal.destroy

    respond_to do |format|
      format.html { redirect_to deals_url }
      format.json { head :ok }
    end
  end
  
  def vouchers
    @subtitle = "Vouchers"
    @intro = "Buyers are granted voucher codes as they purchase your deal."
    @vouchers = @deal.purchased_voucher_codes
    @payments = @deal.payments_authorised
    respond_to do |format|
      format.html
      format.xls do
        render :xls => @payments
                       # :columns => [ :name, :ref ],
                       # :headers => %w[ Name Reference ]
      end
    end
  end
  
  protected
  
    def get_deal_and_company_for_public
      # don't use scoper forthis since we only use for public view
      @deal = Deal.find(params[:id])
      @company = @deal.company 
      # also set to show custom seller header, unless this is the seller/admin
      @show_custom_seller_header = !(current_user && (current_user.admin? || current_user.company == @company))
    end
    
    def get_deal_and_company_and_check_permissions
      # admins can access all deals
      # normal sellesr can only access their own deals
      if current_user && params[:id] 
        potential_deal = Deal.find(params[:id])
        if current_user && (current_user.admin? || current_user.company == potential_deal.company)
          @deal = potential_deal
          @company = @deal.company
        end      
      end
    end
    
    def clean_fb_session
      session[:facebook] = nil
    end
    
    def page_has_map
      @page_has_map = true
    end 
    
    def set_title
      @title = @deal
    end
    

end
