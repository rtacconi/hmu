class DesignController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @company = current_user.company
    @title = @company
    @subtitle = "Customise your deal page"
    @intro = "Customise the look and feel of your deal page template to match your brand."
  end

end
