class FacebookController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
  before_filter :published?, :only => :show
  layout 'facebook'
  
  def index
    # begin        
      DLog.debug "Accessing facebook page a with #{params.inspect}, user is #{current_user}"
      if params[:signed_request].present?

        # for some reason we lose the user here, so let's not do that...
        temp_user = session[:user]
        DLog.debug "Storing user temporary #{session[:user]}"
        @oauth = Koala::Facebook::OAuth.new(
          APP_CONFIG['FBAPPID'], 
          APP_CONFIG['FBAPPSECRET']
        )     
        signed_request = @oauth.parse_signed_request(params[:signed_request])
        DLog.debug "About to view a facebook app page. #{signed_request.inspect}"
         
        # write back the user
        session[:user] = temp_user
        
        if signed_request['page'] && signed_request['page']['id']
          fb_page = FacebookPage.find_by_fb_page_id(signed_request['page']['id'])
          DLog.debug "Found facebook page #{fb_page.inspect}"
          @company = Company.find(fb_page.company_id)
          DLog.debug "Which belongs to company #{@company.inspect}"
          
          current_deal = Deal.current_for_facebook(@company)
          if current_deal
            DLog.debug "Rendering currently active deal, which is #{current_deal.inspect}"
            redirect_to facebook_deal_path(current_deal)
          end
        end
      end                                                 
      if @company
        @title = "#{@company} has no active deals"
      else
        @title = "Sorry"
        @problem_message = "We encountered a problem."        
      end
    # rescue
    #   @problem_message = 'There is a problem with the authorization. Delete this app, login again in your deal and publish again this Tab App'
    # end
  end
  
  # def show
  #   @company = @deal.company
  #   
  #   render :controller => "deals", :action => 'facebook'
  # end  
  
  def create
    @deal = Deal.find(params[:id])
    DLog.debug "Authorising user on facebook", {:user => current_user, :deal => @deal}
    
    temp_user = session[:user]
    @oauth = Koala::Facebook::OAuth.new(
      APP_CONFIG['FBAPPID'], 
      APP_CONFIG['FBAPPSECRET'], fb_callback_url(@deal.id)
    )
    @oauth.get_access_token(params[:code])
    result = @oauth.get_access_token_info(params[:code])
    # write back the user
    session[:user] = temp_user

    if current_user.update_attributes(:access_token => result['access_token'], :access_token_expires => result['expires'])
      DLog.debug "Authorisation succeeded, #{result.inspect}", {:user => current_user, :deal => @deal}
      flash[:notice] = 'You successfully logged in with Facebook. You can set up your deal page below.'
      session[:facebook] = true
    else
      DLog.debug "Facebook authentication failed, #{result.inspect}", {:user => current_user, :deal => @deal}
      flash[:alert] = 'Sorry something went wrong during the process, please try again. You must authorize Hitmeup if you want to publish a deal on your Facebook page.'
    end

    redirect_to publish_deal_path(@deal)
  end
  
  def post_deal_to_wall
    @deal = Deal.find(params[:id])
    @graph = Koala::Facebook::GraphAPI.new(current_user.access_token)
    DLog.debug "Posting deal to facebook wall, graph is #{@graph.inspect}", {:user => current_user, :deal => @deal}
    
    if !Rails.env.production?
      link = "http://staging.hitmeup.co/deals/#{@deal.to_param}"
    else
      link = "http://hitmeup.co/deals/#{@deal.to_param}"
    end

    @graph.put_wall_post(@deal.title, :link => link, 
                         "description" => @deal.description, 
                         :picture => @deal.photo.url(:medium))
    @result = 'ok'
    @deal.posted_times ||= 0
    @deal.update_attribute(:posted_times, @deal.posted_times + 1)
    DLog.debug "Deal has now been posted #{pluralize(@deal.posted_times, 'time')}", {:user => current_user, :deal => @deal}    
    
    respond_to :js
  end
  
  def tabapp_confirm
    begin
      DLog.debug "Adding a facebook tab app", {:user => current_user}    
      page_id = params["tabs_added"].keys.first

      new_page = FacebookPage.find_or_create_by_fb_page_id(
        page_id, 
        :company_id => current_user.company.id
      )   
      new_page.save
      DLog.debug "New facebook page created: #{new_page.inspect}", {:user => current_user}    
          
      @graph = Koala::Facebook::GraphAPI.new(current_user.access_token)
      DLog.debug "New facebook page on graph #{@graph.inspect}", {:user => current_user}    
      
      page = @graph.get_object(page_id)
      DLog.debug  "New facebook page graph page object is #{page.inspect}", {:user => current_user}    
      
      redirect_to "#{page['link']}?sk=app_#{APP_CONFIG['FBAPPID']}"
      
    rescue Exception => e
      @error = e
      render 'confirm_error'
    end
  end

  protected
    
    def published?
      @deal = Deal.find(params[:id])
      flash[:alert] = 'This deal is not available on Facebook'
      redirect_to facebook_path unless @deal.publish_on_facebook
    end
    
    # def find_deals
    #   @deals = Deal.where("publish_on_facebook = ?", true).order("end_time DESC")
    #   @deals = @deals.find_with_index(params[:search]) if params[:search].present?
    # end 
end
