class HomeController < ApplicationController
  before_filter :set_always_show_header
    
  def index
  end
  
  def test
    Notifier.welcome().deliver
    render :text => 'email sent'
  end
  
  def error
    raise 'test'
  end

end
