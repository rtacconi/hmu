class ImageController < ApplicationController
  before_filter :authenticate_user!
  
  def create
    link_code = params[:deal][:link_code]
    picture = Picture.find_by_link_code(link_code)
    picture.destroy if picture
    @picture = Picture.create!(photo: params[:deal][:photo], link_code: link_code)
    
    respond_to :js
  end
  
  def deal_picture
    deal = Deal.find(params[:id])
    @picture = deal.picture
    
    respond_to :js
  end
end
