class PagesController < ApplicationController
  before_filter :require_admin, :except => [:show, :not_found, :whoops]
  before_filter :require_published, :only => :show
  before_filter :set_always_show_header
  
  
  def not_found
  end
  
  def whoops
  end  
  
  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.order("updated_at desc")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pages }
    end
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    if @page.is_blog_post?
      redirect_to blog_path(@page)
    elsif check_static_file(@page.id)
      render "pages/static/#{@page.id}"
    else
      render "show"
    end
  end

  # GET /pages/new
  # GET /pages/new.json
  def new
    @page = Page.create!(title: "Set the title", body: "Conetent goes here")

    respond_to do |format|
      format.html { redirect_to "/editor/pages/#{@page.id}", notice: 'Please enter the content.' }
      format.json { render json: @page }
    end
  end

  # GET /pages/1/edit
  def edit
    @page = Page.find(params[:id])
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(params[:page])

    respond_to do |format|
      if @page.save
        format.html { redirect_to @page, notice: 'Page was successfully created.' }
        format.json { render json: @page, status: :created, location: @page }
      else
        format.html { render action: "new" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pages/1
  # PUT /pages/1.json
  def update
    @page = Page.find(params[:id])

    respond_to do |format|
      if @page.update_attributes(params[:page])
        format.html { redirect_to pages_path, notice: 'Page was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def mercury_update
    page = Page.find(params[:id])
    content = ActiveSupport::JSON.decode(params[:content])
    page.title = content['page_title']['value']
    page.summary = content['page_summary']['value']
    page.body = content['page_content']['value']
    page.save!
    render :text => ''
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page = Page.find(params[:id])
    unless @page.permanent?
      @page.destroy
    else
      flash[:alert] = 'This page is permanent and cannot be changed'
    end

    respond_to do |format|
      format.html { redirect_to pages_url }
      format.json { head :ok }
    end
  end
  
  # GET /pages/not_found
  def not_found
  end
  
  private
  
    def require_published
      @page = Page.find(params[:id])
      return if current_user && current_user.admin?
      
      unless @page.published?
        redirect_to root_url
      end
    end
    
    def check_static_file(id)
      Dir.entries("#{Rails.root}/app/views/pages/static").detect {|f| f == "#{id}.html.haml"}.present?
    end
end
