class PaymentsController < ApplicationController
  layout 'checkout'
  
  def create
    deal = Deal.find(params[:payment][:deal_id])
    @payment = Payment.new(params[:payment])
    @payment.status = :pending
    @payment.amount = deal.price
    if @payment.save
      redirect_to buy_path(params[:payment][:deal_id], :payment_id => @payment.id)
    else
      @deal = Deal.find(params[:payment][:deal_id])
      @credit_card = CreditCard.new
      @countries = Country.all
      render 'checkout/customer_details'
    end
  end
end
