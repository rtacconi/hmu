class ProfileController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @company = current_user.company
  end

end