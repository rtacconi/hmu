class RegistrationsController < Devise::RegistrationsController
  prepend_before_filter :require_no_authentication, :only => [ :new, :create, :cancel ]
  prepend_before_filter :authenticate_scope!, :only => [:edit, :update, :destroy]

  # POST /resource
  def create
    build_resource
    if resource.save
      Notifier.welcome(resource).deliver
      Notifier.new_user(resource).deliver
      sign_in(resource_name, resource)
      redirect_to profile_path
    else
      clean_up_passwords(resource)
      render 'sessions/new'
    end
  end
end
