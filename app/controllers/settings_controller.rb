class SettingsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :require_ssl
  
  def index
    @company = current_user.company
    @setting = @company.setting
    @title = @company
    @subtitle = "Your Settings"
    @intro = "Update your bank details, password and company info here."
  end
  
  def update
    setting = Setting.find(params[:id])
    
    if setting.update_attributes(params[:setting])
      @result = 'Bank account saved.'
    else
      @result = 'error'
    end
  end
  
  def welcome
    render :layout => false
  end
  
  def change_password
    if current_user.update_attributes(params[:user])
      sign_in(current_user, :bypass => true)
      @result = 'Password changed.'
    else
      @result = 'error'
    end
  end
end
