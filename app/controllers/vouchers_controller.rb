class VouchersController < ApplicationController
  before_filter :authenticate_user!
    
  def redeem
    @company = current_user.company if current_user
    @voucher = VoucherCode.get_from_code(@company, params[:voucher][:code]) 
    @voucher = nil unless @voucher &&  @voucher.deal.belongs_to_company?(@company)
    
    if @voucher
      @deal = @voucher.deal 
    
      if !@voucher.bought?
        flash[:alert] = "This voucher hasn't been purchased yet!"
        redirect_to vouchers_deal_path(@deal)
      end
      logger.info "voucher is"
      logger.info @voucher.inspect
    end
    
    

  end
  
  def redeem_now 
    @company = current_user.company if current_user
    @voucher = VoucherCode.find(params[:id])
    @success = @voucher.redeem if @voucher.deal.belongs_to_company?(@company)
    flash[:notice] = "Voucher redeemed!" if @success
  end
end
