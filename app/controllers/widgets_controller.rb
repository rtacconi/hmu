class WidgetsController < ApplicationController
  before_filter :check_key
  KEY = "6463d86fd7c836066afa070562bac960205ab70d"
  
  def show
    metric = params[:id]
    result = Hash.new
    
    if metric == "active_deals"
      result[:item] = Array.new
      result[:item] << {:value =>  Deal.active(Time.now).count, :text => ""}
      result[:item] << {:value =>  Deal.active(7.days.ago).count, :text => ""}
    end
    
    respond_to do |format|
      format.any do
        render :xml => result.to_json
      end
    end    
  end
  
  protected
  
    def check_key
      true
    end
  
end
