module ApplicationHelper
  CCY = '&pound;'   
  
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current sort #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, {:sort => column, :direction => direction}, {:class => css_class}
  end  
  
  def page_title(deal, company, page) 
    title = "HitMeUp"
    default_subheading = "Run your own group buying deals"
    title = "#{company} | #{title}" if company
    title = "#{deal} | #{title}" if deal
    title = "#{page} | #{title}" if page && page.class == Page
    title = "#{title} | #{default_subheading}" unless company || deal || page
    title
  end
  
  def blog_post_path(post)
    "blog/#{post.to_param}"
  end  
  
  def next_friday
    Date.today.end_of_week - 2
  end

  
  def link_to_dynamic_page(title, id, to_blank = false)
    begin  
      page = Page.find(id)
      link_to(title, page_path(Page.find(id)), :target => (to_blank ? "_blank" : nil))
    rescue
      link_to('Page Missing', not_found_pages_path)
    end
  end   
  
  def tab_menu_option(text, link)
    output = "<li"
    output = "#{output} class='active'" if  current_page?(link)
    output = "#{output}>#{link_to(text, link)}</li>"
    output
  end
  
  def button_link(text, link, classes = "btn") 
    output = "<button class='#{classes}'"
    output = "#{output} onclick='location.href = \"#{url_for(link)}\" '"
    output = "#{output}>#{text}</button>"
    output
  end
  
  # format text for display
  # choose whether to display with paragraphs
  def format_text(raw_text, without_paragraphs = false)
    return unless raw_text
    result = raw_text  

    # escape out carrots so they aren't rendered as markup
    result = result.gsub('<', '&#60;').gsub('>', '&#62;')

    # make links in to hyperlinks
    result = result.gsub(/(http[s]?)\:\/{2,2}[^(\)|\s)]+/i) { |match|
      match = match.gsub('<br/>', '')
      external_link(match)
    } 

    # make email addresses mailto:
    result = result.gsub(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i) { |match|
      "<a href='mailto:#{match}'>#{match}</a>"
    }
    
    # change line breaks
    result = result.gsub(/\n/i) { |match|
      "<br/>"
    }


    # # you know the result...
    # if (without_paragraphs)
    #   result = textilize_without_paragraph(result)
    # else
    #   result = textilize(result)
    # end  


    # '<div class="textile">' + result + '</div>' if result.length > 0
    result 
  end  
  
  # make a URL into a HTML link.
  # specify after how many chars to crop links at
  def external_link(url, text = url, crop_links_after_chars = 50, if_not_present_text = nil, if_not_present_link = nil, is_twitter = false)
    # if there's no url and we have if not present text, show the if not present link instead
    if (!url || url.blank?) && if_not_present_text
      url = if_not_present_link
      text = if_not_present_text
    else
      if is_twitter && text[0] != "@"
        url = "http://twitter.com/#{text}"
        text = "@#{text}"  
      end
      http = "http://"
      https = "https://"
      if url
       url = "#{http}#{url}" if url[0..http.length - 1] != http and url[0..https.length - 1] != https
      end
    end
    
    return "-" unless url
    

      
    text = (text.length > crop_links_after_chars) ? "#{text[0..crop_links_after_chars]}..." : text
    link = link_to(text, url , :target => '_blank', :rel => "nofollow", :class => "bold")
    # link = link + " " + image_tag_fp("ext_link.png")
  end  
  
  # money helper
  def to_ccy(amount, no_space = true, show_dash_if_zero = true)
    unit = no_space ? CCY : "#{CCY} ";
    amount == 0 && show_dash_if_zero ? '-' : number_to_currency(amount.to_f, :unit => unit)
  end
  
  def full_date(date, with_day = false, month_and_year_only = false)
    day = ordinal_string = ""
    if !month_and_year_only
      day = with_day ? "%A " : ""
      ordinal_string = ordinal(date) + " "
    end
    date.strftime(day + ordinal_string + "%b %Y")
    # date
  end
  
  def month_and_year(date) 
    date.strftime("%b %Y")
  rescue
    nil
  end

  def short_date(date)
    date.strftime(ordinal(date) + " %b")
  rescue
    nil
  end

  def ordinal(date)
    day_no = date.strftime("%d").to_i
    day_no.to_s + ( (10...20).include?(day_no) ? 'th' : %w{ th st nd rd th th th th th th }[day_no % 10] )
  rescue
    nil
  end
  
  def periods_to_list(periods)
    result = ""
    periods.each do |period| 
      result = "#{result} '#{full_date(period[:start], false, false)}', " 
    end
    result
  end
  
  #writes out key value pairs of data, generated in the controller
  def data_to_list(periods, data)
    result = ""
    periods.each do |period| 
      result = "#{result} #{data[period[:start]]}, " 
    end
    result
  end
  
  
  def friendly_time(datetime, show_full_date_if_before = 3.weeks.ago, say_just_now_for_very_recent_times = true)  
    return "-" unless datetime
    # logger.info "datetime is #{datetime}"
    datetime = datetime.in_time_zone(Time.zone)
    how_long_ago = Time.now - datetime
    # logger.info "datetime is #{datetime} and 1 minute ago is #{1.minute.ago}"
    if datetime > Time.now
      # in the future
      time_in_future(datetime)
    elsif say_just_now_for_very_recent_times && how_long_ago < 1.minute
      "just now"
    elsif show_full_date_if_before and datetime < show_full_date_if_before
      # datetime.strftime("at %I:%M%p on " + ordinal(datetime) + " %b")
      year =  " "+ datetime.strftime("%Y") unless datetime.strftime("%Y") == Date.today.strftime("%Y")
      "#{datetime.strftime("on " + ordinal(datetime) + " %b")}#{year}" 
    elsif how_long_ago < 10.hours
      minutes_ago(datetime)
    else
      when_string = distance_of_time_in_words_to_now(datetime) + " ago"
      when_string = when_string == "1 day ago" ? "yesterday" : when_string
    end
  end
  
  def time_in_future(datetime)
    how_long_to_ago = datetime - Time.now
    if how_long_to_ago < 48.hours
      "in #{minutes_to_go(datetime)}"
    else
      "in #{distance_of_time_in_words_to_now(datetime)}"
    end
  end

  def minutes_to_go(datetime)
    seconds_ago = datetime - Time.now
    #get hours
    hours = 0
    while seconds_ago > 3600
      hours = hours + 1
      seconds_ago = seconds_ago - 3600
    end 
    if hours > 0
      hours_string = pluralize(hours, ' hour') + ' '
    end
    #seconds
    if seconds_ago > 60
      "#{hours_string}#{pluralize((seconds_ago / 60).to_i, ' minute')}"
    else
      "#{hours_string}#{pluralize(seconds_ago.to_i, ' second')} #{hours_string}"
    end
  end
  
  def minutes_ago(datetime)
    seconds_ago = (Time.now - datetime)
    #get hours
    hours = 0
    while seconds_ago > 3600
      hours = hours + 1
      seconds_ago = seconds_ago - 3600
    end 
    if hours > 0
      hours_string = pluralize(hours, ' hour') + ' '
    end
    #seconds
    if seconds_ago > 60
      "#{hours_string}#{pluralize((seconds_ago / 60).to_i, ' minute')} ago"
    else
      "#{hours_string}#{pluralize(seconds_ago.to_i, ' second')} #{hours_string}ago"
    end
  end
        
end
