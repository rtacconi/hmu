module CompanyHelper
  def email_link(company)
    if company.user
      link_to company.user.email, company.user.email
    else
      "missing user"
    end
  end
end