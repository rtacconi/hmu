module DashboardHelper
  def render_company_logo(company, size = :medium, id = 'company_logo')
    if company && company.has_logo?
      image_tag company.logo.url(size), :id => id
    end
  end
  
  
  def render_deal_logo(company, size = :medium, id = 'company_logo')
    if company && company.has_deal_logo?
      image_tag company.deal_logo.url(size), :id => id
    end
  end
  

  
end
