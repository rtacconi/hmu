module DealsHelper
  def with_zero(number)
    if number && number < 10
      "0#{number}"
    else
      "#{number}"
    end
  end
  
  def facebook_login_url(id)
    @oauth.url_for_oauth_code(
      :permissions => 'publish_stream,offline_access,manage_pages,offline_access',
      :display => "popup",
      :redirect_uri => "http://#{request.host_with_port}/facebook_callback/#{id}"
    )
  end
end

