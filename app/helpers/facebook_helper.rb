module FacebookHelper
  def link_to_fb_deal(text, deal)
    link_to text,
            "#{request.protocol}#{APP_CONFIG['SITE']}/facebook/#{deal.to_param}"
  end
end
