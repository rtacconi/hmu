module PagesHelper
  def publish_page(page)
    if page.published?
      link_to "Unpublish", "/pages/#{page.id}?page[published]=false", method: 'put', class: 'btn'
    else
      link_to "Publish", "/pages/#{page.id}?page[published]=true", method: 'put', class: 'btn'
    end
  end
end
