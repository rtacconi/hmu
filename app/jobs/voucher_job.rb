class VoucherJob < Struct.new(:deal_id)
  def perform
    deal = Deal.find(deal_id)
    VoucherCode.generate(deal)
  end
end