class Notifier < ActionMailer::Base
  
  helper :application
  
  default from: "no-reply@hitmeup.co"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.welcome.subject
  #
  def welcome(user)

    mail to: user.email
  end
  
  def new_user(user)
    @user = user
    @company = user.company
    email = APP_CONFIG['SUPPORT_TEAM_ALERTS_TO']
    DLog.info "New user email sent to #{email} and to rtacconi@gmail.com"
    mail to: email,      
         :subject => "[HITMEUP] New User signed up - #{@user}"
    mail to: 'rtacconi@gmail.com',      
         :subject => "[HITMEUP] New User signed up - #{@user}"
  end 
  
  def deal_created_alert(deal)
    @deal = deal
    mail :to => APP_CONFIG['SUPPORT_TEAM_ALERTS_TO'],
         :subject => "[HITMEUP] New Deal Created - #{@deal.title}"
  end
  
  
  def deal_created(deal)
    @deal = deal
    mail :to => @deal.company.user.email,
         :subject => "[HITMEUP] #{@deal.title} - Voucher codes"
  end 
  
 
  
  def order_confirmation(payment)
    @payment = payment
    @deal = Deal.find(payment.deal_id)
    mail :to => payment.email,
         :subject => "[HITMEUP] #{@deal.title} - Order confirmation"
  end
  
  def processed_but_not_tipped(payment)
    @payment = payment
    @deal = Deal.find(payment.deal_id)
    mail :to => payment.email,
         :subject => "[HITMEUP] #{@deal.title} - Not yet tipped"
  end
  
  def voucher(voucher_code)
    @voucher_code = voucher_code
    @payment = @voucher_code.payment
    @deal = @voucher_code.deal
    @company = @deal.company
    
    if @deal.tipped?
      @tipped = "- This deal tipped!"
    else
      @tipped = ""
    end
    
    DLog.info "Sent email-voucher to #{@payment.email}"
    mail :to => @payment.email,
         :subject => "[HITMEUP] #{@deal.title} - Your voucher"
  end
  
  def never_tipped(payment)
    @payment = payment
    @deal = @payment.deal
    @company = @deal.company
    mail :to => @payment.email,
         :subject => "[HITMEUP] #{@deal.title} - Sorry, Didn't Tip"
  end
  
  def failed_payment(payment)
    @payment = payment
    @deal = payment.deal
    mail :to => @payment.email,
         :subject => "[HITMEUP] Failed payment for #{@deal.title}"    
  end
  
  def log_alert(log)
    @log = log
    mail from: "alerts@hitmeup.co",
      to: APP_CONFIG['ERROR_ALERTS_TO'],
      :subject => "[HITMEUP #{@log.level}] #{@log.message[0..50]}"
  end
  
  # view the templates as views
  class Notifier::Preview < MailView
    def voucher
      deal = Deal.last
      payment = Payment.create!(
        transaction_date: Time.now,
        amount: 12.50,
        status: 'paid',
        first_name: 'Jon',
        last_name: 'Doe',
        email: 'jd@ex.com',
        email_confirmation: 'jd@ex.com',
        first_name: 'John',
        last_name: 'Doe',
        billing_address_line1: '4 Regent Street',
        post_code: 'CV31 1EH',
        city: 'Leamington Spa',
        country: 'GB'
      )
      deal.payments << payment
      voucher = VoucherCode.create!(
        deal: deal,
        payment: payment,
        code: "6BPUU8L"
      )
      Notifier.voucher(voucher)
    end
  end
end
