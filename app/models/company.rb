class Company < ActiveRecord::Base
  validates_presence_of :name, :your_name, :phone
  validates :name, :uniqueness => true
  has_one :user
  has_many :deals
  has_many :payments, :through => :deals, :conditions => {:status => :paid}
  has_many :seller_payments
  has_one :setting
  before_create :create_setting
  after_validation :check_confirmation
  validates :description, :word_count => {:less_then => 100}
  
  COMMISSION_PERCENTAGE = 0.15
  
  validates_presence_of :post_code,
                        :address_line1,
                        :on => :update
                             
  # validates :subdomain, :uniqueness => true, :on => :update
  
  # validates_format_of :subdomain, :with => /^\w+$/i, 
  #                     :message => "can only contain letters and numbers.", 
  #                     :on => :update
  
  has_attached_file :logo, { 
                      :styles => { 
                        :small => "24x24#", #crop the small image
                        :medium => "185x80>", #resize the medium iamge
                        :original => "600x600>"
                      }
                    }.merge(Hitmeup::Application::PAPERCLIP_CONFIG)
  # validates_attachment_size :logo, :less_than => 2.megabytes
  
  has_attached_file :deal_logo, { 
                      :styles => { 
                        :small => "24x24#", #crop the small image
                        :medium => "185x80>", #resize the medium iamge
                        :original => "600x600>"
                      }
                    }.merge(Hitmeup::Application::PAPERCLIP_CONFIG)
                    
  has_attached_file :background, {
                      :styles => { 
                        :small => "24x24#", #crop the small image
                        :medium => "185x80>", #resize the medium iamge
                        :original => "600x600>"
                      }
                    }.merge(Hitmeup::Application::PAPERCLIP_CONFIG)
  validates_attachment_size :background, :less_than => 3.megabyte
                        
  def has_logo?
    self.logo.present? && !self.logo.url.include?("missing.png")
  end
  
  def has_deal_logo?
    self.deal_logo.present? && !self.deal_logo.url.include?("missing.png")
  end
  
  def has_background?
    self.background.present? && !self.background.url.include?("missing.png")
  end
  
  def profile_completed?
    if name.present? && your_name.present? &&  phone.present? && post_code.present? && address_line1.present? && logo.present?
      true
    else
      false
    end
  end
  
  def design_completed?
    has_deal_logo? && has_background? && font_colour.present?
  end
  
  def bank_details_present?
    setting.bank_name.present? && setting.branch.present? && setting.sort_code.present? &&   setting.account_number.present? 
  end
  
  def has_deals?
    deals.count > 1
  end
  
  def create_setting
    self.setting = Setting.create!
  end
  
  def check_confirmation
    return if confirmation_state == "design_completed"
    
    # details_completed is called by Setting 
    
    if confirmation_state == "profile_completed"
      confirmation.trigger(:design_completed)
      return
    end
    
    if confirmation_state == "not_completed"
      confirmation.trigger(:profile_completed)
      return
    end
    
    if confirmation_state.nil?
      confirmation.trigger(:not_completed)
    end
  end
  
  def total_revenue
    payments.sum(:amount)
  end
  
  def revenue(since = nil, up_until = nil)
    if since && up_until    
      up_until = up_until + 1.day
      conditions = ["transaction_date > ? and transaction_date < ? ", since, up_until]      
    elsif since
      conditions = ["transaction_date > ? ",  since]
    else
      conditions = nil
    end
    self.payments.sum(:amount, :conditions => conditions)
  end
  
  def revenue_after_commission(since = nil, up_until = nil)
    revenue(since, up_until) * (1 - COMMISSION_PERCENTAGE)
  end
        

  def total_fees
    total_revenue * COMMISSION_PERCENTAGE
  end
  
  def seller_balance
    total_revenue - total_fees - revenue_paid
  end  
  
  def revenue_paid
    seller_payments.sum(:amount)
  end
  
  def address
    # although required, some post code could be nil (old records)
    upcased_post_code = post_code.upcase if post_code.present?
    [address_line1, address_line2, city, upcased_post_code].reject! { |c| c.blank? }.join(", ")
  rescue
    nil
  end
    
  def bank_details_present?
    setting.bank_name.present? && setting.branch.present?  &&     setting.sort_code.present?   &&   setting.account_number.present? 
  end
  
  def to_s
    name
  end  
  
  
  def self.with_outstanding_revenue
    all.delete_if {|company| company.seller_balance == 0} 
  end
  
  def confirmation
    @confirmation ||= begin
      fsm = MicroMachine.new(confirmation_state)
      
      fsm.when(:not_completed, nil => "not_completed")  # when saving
      fsm.when(:profile_completed, "not_completed" => "profile_completed")
      fsm.when(:design_completed, "profile_completed" => "design_completed")
      fsm.when(:details_completed, "design_completed" => "details_completed")

      fsm.on(:any) { self.confirmation_state = confirmation.state }

      fsm
    end
  end
  
  def persist_confirmation
    update_attribute(:confirmation_state, confirmation.state)
  end
end
