class DLog < ActiveRecord::Base
  
  belongs_to :deal
  belongs_to :payment
  belongs_to :user
  
  acts_as_indexed :fields => [:message]
  
  def self.warnings_and_errors
    where(["level = ? or level = ?", "Warning", "Error"])
  end
  
  def self.errors
    where(["level = ?", "Error"])
  end
  
  def self.debug(message, objects = nil)
    log(message, "Debug", objects) if Rails.env.development?
  end
    
  def self.info(message, objects = nil)
    log(message, "Info", objects)
  end
  
  def self.warn(message, objects = nil)
    log(message, "Warning", objects)
    # now notify of warning
    
  end
  
  def self.error(message, objects = nil)
    log(message, "Error", objects)
    # now notify of error
    #
  end

  
  def self.log(message, level, objects)
    record = self.new(:level => level, :message => message)
    if objects
      record[:deal_id] = objects[:deal].id if objects[:deal]
      record[:payment_id] = objects[:payment].id if objects[:payment]
      record[:user_id] = objects[:user].id if objects[:user]
      if objects[:request]
        record[:ip] = objects[:request].remote_ip
        record[:url] = objects[:request].url
      end
    end
    record.save
    puts record
  end
  
  
  def mark_notified
    update_attribute(:notified, true)
  end
  
  def when
    self.created_at.strftime("%m/%d/%Y %H:%M.%S")
  end
  
  
  def to_s
    objects = Array.new
    objects << deal.to_log if deal
    objects << payment.to_log if payment
    objects << user.to_log if user
    context = "for #{objects.to_sentence}" if objects.count > 0
    "#{self.when} #{self.level} - #{self.message} #{context}"
  end
  
  def self.truncate_to(leaving)
    number_to_delete = self.count - leaving - 1
    delete_to = find(:all, :order => "id desc", :limit => leaving)[leaving - 1]
    if delete_to
      delete_all(["id < ?", delete_to.id])
      info "Deleted #{number_to_delete} log entries"
    end
  end
  
  def self.errors_since(date)
    self.count(:conditions => ["created_at > ? and level = ?", date, "Warning"])
  end
  
  def self.last_error
    self.find(:first, :conditions => ["level = ?", "Error"], :limit => 1, :order => "id desc")
  end
  
  def self.send_alerts(for_level)
    entries = self.find(:all, :conditions => ["level = ? and notified = ?", for_level, false], :order => "id")    
    entries.each do |entry|
      Notifier.log_alert(entry).deliver
      entry.mark_notified
    end  
  end  
  
  # def self.truncate_to(leaving)
  #   number_to_delete = RLog.count - leaving - 1
  #   delete_to = RLog.find(:all, :order => "id desc", :limit => leaving)[leaving - 1]
  #   if delete_to
  #     self.delete_all(["id < ?", delete_to.id])
  #     RLog.info self, "Deleted #{number_to_delete} log entries"
  #   end
  # end  
  
end

# == Schema Information
#
# Table name: r_logs
#
#  id         :integer         not null, primary key
#  level      :string(255)
#  event      :string(255)
#  message    :text
#  account_id :integer
#  created_at :datetime
#  updated_at :datetime
#

