class Deal < ActiveRecord::Base
  belongs_to :company
  has_many :voucher_codes, :dependent => :destroy
  has_many :payments
  
  acts_as_indexed :fields => [:title, :description, :small_print]
  
  validates_presence_of :company, :on => :create
  validates_presence_of :title, :description, :small_print, :price, :rrp,
                        :quantity, :min_quantity, :start_time, :end_time
  before_create :set_parameters
  before_save :set_picture
  after_create :send_emails
  after_save :add_or_remove_voucher_codes_if_required
  validate :min_max_quantity
  validate :limit_quantity
  validate :start_end_time
  validate :start_time_not_in__the_past, :on => :create
  validate :time_range
  validate :check_bargin
  
  MAX_NUMBER_OF_VOUCHERS_PER_DEAL = 10000
   
  # validation methods

  def limit_quantity
    errors.add(:quantity, "can't be greater than #{MAX_NUMBER_OF_VOUCHERS_PER_DEAL}") if quantity && quantity > MAX_NUMBER_OF_VOUCHERS_PER_DEAL
  end
    
  def min_max_quantity
    errors.add(:min_quantity, "can't be greater than quantity") if min_quantity && quantity && min_quantity > quantity
  end
  
  
  def check_bargin
    if self.rrp && self.price && self.rrp <= self.price
      errors.add(:rrp, "This is no bargain! RRP must be more than the deal price.")
    end
  end
  
  def start_end_time
    if self.start_time && self.end_time && self.start_time > self.end_time
      errors.add(:end_time, "End time can't be before start time")
    end
  end
  
  def start_time_not_in__the_past
    if self.start_time && self.start_time < Time.now
      errors.add(:start_time, "Start time can't be in the past")
    end
  end
  
  def time_range
    if self.end_time && self.start_time && Time.diff(self.end_time.to_time, self.start_time.to_time)[:day] > 3
      errors.add(:start_time, "start date and the end date should not be more than 3 days apart")
    end
  end
  
  
  # status
  
  def status
    if blocked
      "Blocked"
    elsif !published
      "Not published"
    elsif !started?
      "Not started"  
    elsif ended? && no_tipping_point?
      "Ended"
    elsif ended? && tipped?
      "Ended, tipped"      
    elsif ended? && !tipped?
      "Ended, not tipped"
    elsif ended? && tipped?
      "Ended, tipped"
    elsif all_vouchers_purchased?
      "All purchased"
    elsif active? && no_tipping_point?
      "Active, no tipping point"            
    elsif active? && tipped?
      "Active, tipped"
    elsif active? && !tipped?
      "Active, not yet tipped"
    end
  end

  def is_editable?
    !started?
  end
    
  def available?
    active? && vouchers_remaining?
  end
  
  def active?
    started? && !ended?
  end
  
  def ended?
    Time.now > self.end_time if self.end_time
  end
  
  def started?
     !new_record? && Time.now >= self.start_time if self.start_time
  end
  
  
  # geocoding
  geocoded_by :address
  after_validation :geocode
  
  def address
    "#{street}, #{post_code}, UK"
  end
  # end geocoding block
  

  # attachments
  has_one :picture, :as => :imageable
  
  def photo
    picture.photo if picture
  end
  
  
  # tipping and voucher logic 
  
  def set_parameters
    self.published = true
    self.expiry_time = 2.months.since(self.end_time)
  end
  
  def set_picture
    pic = Picture.find_by_link_code(link_code)
    self.picture = pic if pic
  end
  
  def remaining
    quantity - payments_authenticated.count
  end
  
  def all_vouchers_purchased?
    remaining <= 0
  end
  
  def tipped?
    min_quantity > 0 && payments_authenticated.count >= min_quantity
  end
  
  def purchases_required_to_tip
     min_quantity - payments_authenticated.count
   end
  
  def has_tipping_point?
    !no_tipping_point?
  end
  
  def no_tipping_point?    
    min_quantity <= 1
  end
  
  def marked_tipped
    if !tipped_at
      self.update_attribute(:tipped_at, Time.now)
    end
  end
  
  
  def vouchers_remaining?
    remaining > 0
  end
  
  def due_days
    # need to ensure we include the weeks in days, since we don't print out weeks
    due_date[:day] + (due_date[:week] * 7) if due_date
  end
  
  def due_hours
    due_date[:hour] if due_date
  end
  
  def due_minutes
    due_date[:minute] if due_date
  end
  
  def due_seconds
    due_date[:second] if due_date
  end
  
  def due_date
    # due date is end date if the deal hasn't started, or days until it starts if it's not yet started
    if started?
      Time.diff(Time.now, end_time) if end_time
    else
      Time.diff(start_time, Time.now) if start_time
    end
  end
  
  def self.past(company)
    where("company_id=? AND end_time <=?", company.id, Time.now).order('end_time ASC')
  end
  
  def self.future(company)
    where("company_id=? AND start_time > ?", company.id, Time.now).order('end_time ASC')
  end
  
  def self.current(company)
    where("company_id=? AND end_time >?", company.id, Time.now).order('end_time ASC')
  end
  
  # a deal that is now or in the future to show in the facebook page
  def self.current_for_facebook(company)
    result = nil
    # get next ending deal
    where("company_id=? AND end_time > ?", company.id, Time.now).order('end_time').each do |d|
      # loop through each until we find one with vouchers remaining
      result = d if d.vouchers_remaining? && !result
    end        
    result
  end
  
  
  def discount
    ((1 - (price / rrp)) * 100).to_i
  end
  
  def payments_authorised
    # aka authenticate
    # payments that have been captured/confirmed
    Payment.where("deal_id = ? AND status = ?", self.id, :paid)
  end
  
  def payments_authenticated
    # if it has a token, payment authencitated successsfully
    # payments that have gone through checkout and authenticated successfully
    Payment.where("deal_id = ? AND token is not ? and status <> ? and status <> ?", self.id, nil, :failed, :refunded)
  end
  
  def all_payments_processed?
    payments_pending.count == 0
  end
  
  # not processed yet
  def failed_payments
    Payment.where("deal_id = ? AND status = ?", self.id, :failed)
  end
  
  # not processed yet
  def payments_pending
    Payment.where("deal_id = ? AND status = ?", self.id, :pending)
  end
  
  
  def self.not_blocked
    where("blocked IS null OR blocked = ?", false)
  end

  
  # used to avoid processing old deals and improve performance
  def block!
    update_attribute(:blocked, true)
  end
  
  def belongs_to_company?(co)
    self.company == co
  end
  
  
  def send_emails
    if company
      Notifier.deal_created(self).deliver
      Notifier.deal_created_alert(self).deliver
    end
  end
  
  def add_or_remove_voucher_codes_if_required
    Delayed::Job.enqueue(VoucherJob.new(self.id))
  end
  
  def purchased_voucher_codes
    self.voucher_codes.find(:all, :conditions => ["payment_id is not ?", nil], :include => :payment)
  end
  
  def revenue(since = nil, up_until = nil)
    if since && up_until    
      up_until = up_until + 1.day
      conditions = ["deal_id = ? and transaction_date >= ? and transaction_date < ? and status = ?", self.id, since, up_until, :paid]      
    elsif since
      conditions = ["deal_id = ? and transaction_date >= ? and status = ?", self.id, since, :paid]
    else
      conditions = ["deal_id = ? and status = ?", self.id, :paid]
    end
    Payment.sum(:amount, :conditions => conditions)
  end
  
  def revenue_after_commission(since = nil, up_until = nil)
    revenue(since, up_until) * (1 - Company::COMMISSION_PERCENTAGE)
  end

  
  def to_param
    if title.nil?
      "#{id}"
    else
      "#{id}-#{title.parameterize}"
    end
  end
  
  def to_s
    title ? title : "New Deal"
  end
  
  def to_log
    "#{title} (Deal ID #{id})"
  end
  
  
  # class methods
  
  
  # implementation of our search features
  def self.search(search)  
    if search  
      where('lower(title) LIKE ?', "%#{search.downcase}%")  
    else  
      self  
    end  
  end
  
  def self.facebook
    where("publish_on_facebook = ?", true)
  end  
  
  def self.past_count
    where("end_time <?", Time.now).count
  end
  
  def self.future_count
    where("start_time > ?", Time.now).count
  end 
  
  def self.active_count
    self.active(Time.now).count
  end
  
  def self.active(at_time)
    where("end_time > ? and start_time <= ?", at_time, at_time)
  end

  def not_expired?
    Time.now < expiry_time
  end
end
