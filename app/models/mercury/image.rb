class Mercury::Image < ActiveRecord::Base

  self.table_name = :mercury_images

  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  
  has_attached_file :image, { 
                      :styles => {
                        :medium => "300x300>",
                        :medium => "300x300>",
                        :original => "600x600>"
                      }
                    }.merge(Hitmeup::Application::PAPERCLIP_CONFIG)

  delegate :url, :to => :image

  def serializable_hash(options = nil)
    options ||= {}
    options[:methods] ||= []
    options[:methods] << :url
    super(options)
  end

end