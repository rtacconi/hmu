class Page < ActiveRecord::Base
  belongs_to :section
  validate :title, presence: true
  validate :body, presence: true
  
  def self.find_posts
    where(section_id: 1).order("created_at DESC")
  end
  
  def self.published
    where(published: true)
  end
  
  def to_param
    if title.nil?
      "#{id}"
    else
      "#{id}-#{title.parameterize}"
    end
  end  
  
  def is_blog_post?
    section && section.id == 1
  end
  
  def to_s
    title
  end
end
