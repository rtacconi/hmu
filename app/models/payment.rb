class Payment < ActiveRecord::Base
  has_one :voucher_code
  belongs_to :deal
  enum_attr :gateway, %w(sagepay)
  enum_attr :status, %w(pending paid failed refunded)
  
  validates :amount, :presence => true, :on => :update
  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :email, :presence => true
  validates :email_confirmation, :presence => true
  validates :billing_address_line1, :presence => true
  validates :post_code, :presence => true
  validates :city, :presence => true
  validates :country, :presence => true  
  
  def to_s
    "Payment #{id} (#{gateway}, amount #{amount}, user #{user_id}, email #{email}, name #{name})"
  end
  
  def to_log
    to_s
  end
  
  def name
    "#{self.first_name} #{self.last_name}"
  end
  
  def authenticated?
    # returns if this payment has made it through the checkout
    # which we can determine by the presence of a token
    !!token
  end
  
  def refund_available?
    status == :paid && !refunded? && (!voucher_code || !voucher_code.redeemed)                         
  end
     
  
  def refund!
    if refund_available?
      update_attribute(:status, :refunded)
      voucher_code.update_attribute(:payment_id, nil)
    end
  end
  
  def refunded?
    status == :refunded
  end
  
  def self.process(deals)  
    start = Time.now 
    DLog.info "Going to begin processing all deals... "
    deals.each do |deal|
      throw "You cannot process a blocked deal" if deal.blocked
      # DLog.info "Processing payments ", {:deal => deal}
      
      if !deal.has_tipping_point?
        DLog.info "Deal has no tipping point ", {:deal => deal}
        self.delivery(deal)
      else
        if deal.tipped?
          DLog.info "Deal has tipped ", {:deal => deal}       
          # update the deal with a tipped_at date
          deal.marked_tipped
          self.delivery(deal)
        else
          # deal hasn't tipped                              
          if deal.ended?
            # if the deal has ended, we can block it
            DLog.info "Deal never tipped, freezing deals and letting buyers know  ", {:deal => deal}
            deal.block! 
            # if the deal never tipped,let everyone know here...
            deal.payments_authenticated.each do |p|
              p.update_attributes(:deal_never_tipped_sent => Time.now)
              Notifier.never_tipped(p).deliver
            end
          else
            DLog.info "Deal not yet tipped, #{deal.payments_authenticated.count} vouchers bought and tipping point is #{deal.min_quantity} ", {:deal => deal}
            self.not_tipped(deal)
          end
        end
      end
    end 
    seconds = Time.now - start
    DLog.info "...processing all deals done in #{seconds} seconds"
  end
  
  # process only pending payments and block the deal if possible
  def self.delivery(deal) 
    DLog.info "Processing deliveries  ", {:deal => deal}
    
    # we only want to process payments that have made it through the checkout i.e. authenticated
    deal.payments_authenticated.each do |payment|
      # deal with pending payments
      if payment.status == :pending
        DLog.info "Processing payment with status #{payment.status}   ", {:deal => deal, :payment => payment}
        
        # capture the payment
        if Rails.env.development?
          payment_successful = true
        else 
          # need to times price by 100 to convert from pounds to pence e.g. 2.00 becomes 200p
          response = SpreedlyCore.capture(payment.token, payment.amount * 100)          
          DLog.info "Payment response from Spreedly for #{payment.amount} and  token #{payment.token} was #{response.inspect}", {:deal => deal, :payment => payment}
          payment_successful = response.code == 200
        end
        DLog.info "Response for payment capture was #{payment_successful}", {:deal => deal, :payment => payment}
        
        if payment_successful
          DLog.info "Payment capture successful  ", {:deal => deal, :payment => payment}
          payment.update_attributes!(:status => :paid)
          
          if VoucherCode.delivery(payment)
            DLog.info "Status updated to paid and voucher sent, sending voucher  ", {:deal => deal, :payment => payment}
            payment.update_attributes!(:voucher_sent => Time.now)
          else
            DLog.error "Failed to send voucher  ", {:deal => deal, :payment => payment}            
          end

        else
          DLog.warn "Payment failed  ", {:deal => deal, :payment => payment}
          payment.update_attributes(:status => :failed, :payment_failed_sent => Time.now)
          Notifier.failed_payment(payment).deliver
        end
      end
    end
    
    DLog.info "Processing deliveries for #{deal} ended, there are #{deal.payments_authenticated.count} payments authenticated and #{deal.payments_authorised.count} payments authorised and #{deal.payments_pending.count} pending ", {:deal => deal}
    
    if deal.all_payments_processed? && deal.ended?
      DLog.info "All payments processed and deal expired, freezing deal  ", {:deal => deal}
      deal.block! 
    end
  end
  
  def self.not_tipped(deal)
    deal.payments.each do |payment| 
      # send not tipped payments if the deal still hasn't tipped
      # 24 hours after purchase
      if payment.status == :pending && !payment.not_tipped_sent && payment.created_at < 24.hours.ago
        begin
          Notifier.processed_but_not_tipped(payment).deliver
          payment.update_attribute(:not_tipped_sent, Time.now)
        rescue
          DLog.warn "Couldn't sent not yet tipped email!  ", {:deal => deal, :payment => payment}
        end
      end
    end
  end
  
  
  def self.revenue(since = nil, up_until = nil)
    if since && up_until
      conditions = ["transaction_date > ? and transaction_date <= ? and status = ?", since, up_until, :paid]      
    elsif since
      conditions = ["transaction_date > ? and status = ?",  since, :paid]
    end
    Payment.sum(:amount, :conditions => conditions)
  end
  
  def self.all_since(date = nil)
    if date
      Payment.sum(:amount, :conditions => ["status = ? and transaction_date > ?", :paid, date])      
    else
      Payment.sum(:amount, :conditions => {:status => :paid})
    end
  end
  
  def self.pending_count 
    Payment.count(:conditions => ["status = ?", :pending])      
  end
  
end
