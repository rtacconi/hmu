class Picture < ActiveRecord::Base
  belongs_to :imageable, :polymorphic => true
  
  has_attached_file :photo, { 
                      :styles => { 
                        :small => "24x24#", #crop the small image
                        :medium => "185x80#", #resize the medium iamge
                        :large => "430x430#",
                        :original => "600x600#"
                      }
                    }.merge(Hitmeup::Application::PAPERCLIP_CONFIG)
  validates_attachment_size :photo, :less_than => 2.megabytes
end