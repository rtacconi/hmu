class Setting < ActiveRecord::Base
  belongs_to :company
  
  after_update :check_confirmation
  
  def check_confirmation
    return unless company
    
    if company.confirmation_state == "design_completed" && company.bank_details_present?
      company.confirmation.trigger(:details_completed)
      company.persist_confirmation # save the new state
    end
  end
end
