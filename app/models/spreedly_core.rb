require 'httparty'

class SpreedlyCore

  def self.config_file
    if Rails.env == 'production' || Rails.env == 'staging'
      File.expand_path(Rails.root.join('config', 'spreedly_core.yml'))
    else
      File.expand_path(Rails.root.join('config', 'spreedly_core_development.yml'))
    end
  end

  def self.config
    @config ||= YAML.load(ERB.new(File.read(config_file)).result).with_indifferent_access
  end


  include HTTParty
  headers 'Accept' => 'text/xml'
  headers 'Content-Type' => 'text/xml'
  basic_auth(config[:api_login], config[:api_secret])
  base_uri("#{config[:core_domain]}/v1")
  format :xml


  def self.api_login
    config[:api_login]
  end

  def self.purchase(payment_method_token, amount, currency_code="GBP")
    post_gateways("purchase", payment_method_token, amount, currency_code)
  end

  def self.authorize(payment_method_token, amount, currency_code="GBP")
    post_gateways("authorize", payment_method_token, amount, currency_code)
  end

  def self.capture(payment_method_token, amount, currency_code="GBP")
    post_transactions("capture", payment_method_token, amount, currency_code)
  end

  def self.void(payment_method_token, amount, currency_code="GBP")
    post_transactions("void", payment_method_token, amount, currency_code)
  end

  def self.get_payment_method(token)
    self.get("/payment_methods/#{token}.xml")
  end

  def self.add_payment_method_url
    "#{config[:core_domain]}/v1/payment_methods"
  end

  private
    def self.to_xml_params(hash)
      hash.collect do |key, value|
        tag = key.to_s.tr('_', '-')
        result = "<#{tag}>"
        if value.is_a?(Hash)
          result << to_xml_params(value)
        else
          result << value.to_s
        end
        result << "</#{tag}>"
        result
      end.join('')
    end

    def self.post_gateways(action, payment_method_token, amount, currency_code="GBP")
      transaction = { :amount => amount, :currency_code => currency_code, :payment_method_token => payment_method_token }
      self.post("/gateways/#{config[:gateway_token]}/#{action}.xml", :body => self.to_xml_params(:transaction => transaction))
    end
    
    def self.post_transactions(action, payment_method_token, amount, currency_code="GBP")
      transaction = { :amount => amount, :currency_code => currency_code }
      self.post("/transactions/#{payment_method_token}/#{action}.xml", :body => '')
    end

end
