class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :company_attributes,
                  :access_token, :access_token_expires
  validates_presence_of :email
  belongs_to :company
  accepts_nested_attributes_for :company
  
  def store_fb_token(result)
    self.update_attributes(:access_token => result['access_token'], 
                           :access_token_expires => result['expires'])
  end
  
  def can_edit_deal?(deal)
    self.admin? || deal.company == self.company
  end
  
  def to_s
    self.company.name
  end
  
  def to_log
    "#{email} (#{company.name})"
  end
end
