class VoucherCode < ActiveRecord::Base
  belongs_to :deal
  belongs_to :payment
  
  def self.delivery(payment)
    deal = Deal.find(payment.deal_id)
    DLog.info "Assigning voucher code for ", {:deal => deal, :payment => payment}
    
    # get the next voucher code that is for this deal and has no payment against it, also check it's not been redeemed
    voucher_code = where("deal_id = ? AND payment_id is null and redeemed = ?", deal.id, false).first
    if voucher_code
      DLog.info "Voucher code #{voucher_code.inspect} assigned", {:deal => deal, :payment => payment}
      voucher_code.update_attributes(:payment_id => payment.id)
      Notifier.voucher(voucher_code).deliver
      true
    else
      DLog.error "No vouchers remaining!", {:deal => deal, :payment => payment}
      false
    end
  end

  # we do not need AR transactions because this method is run only once
  # and uniqueness is only for a deal.id and a generated code
  def self.generate(deal)
    DLog.info "Going to generate vouchers", {:deal => deal}
    desired_qty = deal.quantity
    current_qty = self.count(:conditions => ["deal_id = ?", deal.id])
    if (desired_qty < current_qty)
      # delete extra vouchers, since we have over the desired quantity
      # need to make sure they're unbought and unused codes
      DLog.info "Deleting unused and unbought vouchers now that the deal quantity is less", {:deal => deal}
      self.delete_all(["deal_id = ? and redeemed = ? and payment_id is null", deal.id, false])
      # then get the new current_qty, which should be less than desired qty
      current_qty = self.count(:conditions => ["deal_id = ?", deal.id])
    end
    
    if (desired_qty > current_qty)
      qty_to_generate = desired_qty - current_qty
      DLog.info "Going to generate #{qty_to_generate} vouchers so we have #{desired_qty} in total", {:deal => deal}

      count = 0
      (1..qty_to_generate).each do
        count = count + 1
        DLog.debug "Generated #{count} new codes", {:deal => deal} if count % 100 == 0
        create!(:code => generate_code, :deal => deal)
      end
      current_qty = self.count(:conditions => ["deal_id = ?", deal.id])
      DLog.info "Voucher generation complete, we have now have #{current_qty}", {:deal => deal}
    end

  end 
  
  def self.get_from_code(company, code)
    voucher = self.find(:first, :conditions => {:code => code} )
    # check this voucher belongs to company
    (voucher && voucher.deal.company == company) && voucher.deal.not_expired? ? voucher : nil
  end
  
  def self.use(company, code)
    deal = get_deal_with_code(company, code)
  end
  
  def redeem
    # only let use once...
    return false if self.redeemed
    # otherwise expire...
    update_attribute(:redeemed, true)
  end
  
  def bought?
    !!self.payment_id
  end
  
  def status
    if redeemed
      "Redeemed"
    elsif bought?
      "Assigned"
    else
      "Available"
    end
  end
  
  
  def to_s
    "#{code} (#{status})"
  end
  

  
  
  
  private

    
    def self.generate_code
      alpha_numerics = [('0'..'9'),('A'..'Z')].map {|range| range.to_a}.flatten
      new_code = (0..6).map { 
        alpha_numerics[Kernel.rand(alpha_numerics.size)]
      }.join
      # if this code already exists, find another
      new_code = generate_code if find_by_code(new_code)
      new_code
    end


end
