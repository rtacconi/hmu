class WordCountValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if options[:less_then].present?
      return if value.nil?
      unless value.words < options[:less_then].to_i
        record.errors.add attribute, "must be less then #{options[:less_then]}  words"
      end
      return
    end
    
    if options[:grater_then].present?
      unless value.words > options[:grater_then].to_i
        record.errors.add attribute, "must be greater then #{options[:grater_then]} words"
      end
      return
    end
    
    raise "not wrking"
  end
end