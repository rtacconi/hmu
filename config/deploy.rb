require "bundler/capistrano"
#load 'deploy/assets'


set :application, "hitmeup"
set :user, 'ubuntu'
ssh_options[:keys] = [File.join(ENV['HOME'], '.ssh', 'riccardo.pem')]
set :repository,  "git@github.com:WeMakeWebsites/hitmeup.git"
set :scm, :git
set :scm_verbose, true
set :deploy_via, :remote_cache
set :use_sudo, false

ssh_options[:forward_agent] = true
default_run_options[:pty] = true

task :production do
  set :keep_releases, 2
  set :deploy_to, "/home/ubuntu/production"
  set :branch, "master"
  set :rails_env, 'production'
  set :homepage_url, 'http://hitmeup.co/'
  role :app, "79.125.19.191"
  role :web, "79.125.19.191"
  role :db,  "79.125.19.191", :primary => true
end

task :staging do
  set :keep_releases, 2
  set :deploy_to, "/home/ubuntu/staging"
  set :branch, "staging"
  set :rails_env, 'staging'
  set :homepage_url, 'http://hitmeup.wemakewebsites.com/'
  role :app, "54.247.162.130"
  role :web, "54.247.162.130"
  role :db,  "54.247.162.130", :primary => true
end 

task :snow do
  set :keep_releases, 2
  set :deploy_to, "/home/ubuntu/snow"
  set :branch, "snow"
  set :rails_env, 'staging'
  set :homepage_url, 'http://hitmeup.wemakewebsites.com/'
  role :app, "54.247.162.130"
  role :web, "54.247.162.130"
  role :db,  "54.247.162.130", :primary => true
end

# whenever for rebuilding the crontab from schedule.rb
# first two lines here provide environment-specific support (so crontab not overwritten with each release)
set :whenever_environment, defer { rails_env }
set :whenever_identifier, defer { "#{application}_#{rails_env}" }
set :whenever_command, "bundle exec whenever"
require "whenever/capistrano"

namespace :deploy do
 # Using Phusion Passenger
 [:stop, :start, :restart].each do |task_name|
   task task_name, :roles => [:app] do
     run "cd #{current_path} && touch tmp/restart.txt"
   end
 end

 task :symlink_configs do
   run %( ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml)
   run "ln -nfs #{shared_path}/pids #{release_path}/tmp/pids"
 end

 desc "bundle gems (application only, i.e., no test gems)"
 task :bundle do
   run(
     "cd #{release_path} && RAILS_ENV=#{rails_env} && bundle install --deployment --without development test mac --path #{shared_path}/gems"
   )
   run("cd #{release_path} && RAILS_ENV=#{rails_env} && bundle exec rake db:migrate RAILS_ENV=#{rails_env}")
 end
 
 desc 'Precompile assets'
 task :precompile do
   run("cd #{release_path} && RAILS_ENV=#{rails_env} && bundle exec rake assets:precompile RAILS_ENV=#{rails_env} --trace")
 end 
                                        
 
  #  
  # task :precompile, :roles => :web, :except => { :no_release => true } do
  #   from = source.next_revision(current_revision)
  #   if capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0
  #     run %Q{cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} #{asset_env} assets:precompile}
  #   else
  #     logger.info "Skipping asset pre-compilation because there were no asset changes"
  #   end
  # end   
  
     
end

# Bluepill related tasks
after "deploy:update", "bluepill:quit", "bluepill:start"
namespace :bluepill do
  desc "Stop processes that bluepill is monitoring and quit bluepill"
  task :quit, :roles => [:app] do
    run "sudo bluepill stop || true"
    run "sudo bluepill quit || true"
  end
 
  desc "Load bluepill configuration and start it"
  task :start, :roles => [:app] do
    run "sudo bluepill load #{deploy_to}/current/config/#{rails_env}.pill"
  end
 
  desc "Prints bluepills monitored processes statuses"
  task :status, :roles => [:app] do
    run "sudo bluepill status"
  end
end

namespace :cache do
  desc 'Warm up cache'
  task :warm_up do
    # add -r to make it recursive
    run("wget -nd --delete-after #{homepage_url}")
  end
end

after "deploy:update_code" do
 deploy.symlink_configs
 deploy.bundle
 deploy.precompile
 deploy.cleanup
end

after "deploy" do
 cache.warm_up
end