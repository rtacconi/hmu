class String
  def words
    self.scan(/(\w|-)+/).size
  end
end