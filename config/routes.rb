Hitmeup::Application.routes.draw do     

  # DLog.info "Setting URL to #{APP_CONFIG['SITE']}"
  
  # re-write non-www urls
  # constraints(:host => "www.hitmeup.co") do
  #   # Won't match root path without brackets around "*x". (using Rails 3.0.3)
  #   match "(*x)" => redirect { |params, request|
  #     URI.parse(request.url).tap { |x| x.host = "hitmeup.co" }.to_s
  #   }
  # end     
  
  match 'welcome' => 'settings#welcome'
  
  get "blog" => "blog#index"
  match '/blog/:id' => 'blog#show'
  get '/' => "blog#index", constraints: { subdomain: /blog/ }
  

  Mercury::Engine.routes
  
  match '/image' => 'image#create'
  
  match '/image/deal_picture/:id' => 'image#deal_picture'
  
  get '/payments' => 'checkout#index'
  
  match '/buy/:id' => 'checkout#show', :as => :buy
  
  match '/confirm/:id' => 'checkout#confirm', :as => :confirm

  match "/design" => 'design#index'
  
  match '/facebook_callback/:id' => 'facebook#create'
  
  match '/facebook' => 'facebook#index'
  
  match '/tabapp_confirm' => 'facebook#tabapp_confirm'
  
  match '/facebook/:id' => 'facebook#show'
  
  match '/dashboard' => "dashboard#index"
  
  match '/revenue' => "dashboard#revenue"
  
  match '/dashboard' => "dashboard#index", :as => :user_root
  
  match '/profile' => "profile#index"
  
  get "/home/index"
  
  get "/home/test"
  
  get "/home/error"
  
  get "/checkout/test" 
  
  match "/facebook/post_deal_to_wall/:id" => "facebook#post_deal_to_wall", :as => :post_deal_to_wall
  
  # geckoboard widgets
  match '/widgets/:id' => 'widgets#show'
  
  # test HTML emails
  unless Rails.env.production?
    mount Notifier::Preview => 'notifier_preview'
  end
  
  # RESTful resources here

  resources :settings do
    collection do
      put :change_password
    end
  end

  namespace :admin do
    resources :moderations
    resources :analytics
    resources :dashboard
    resources :users
    resources :payments do
      member do
        get 'refund' 
        post 'refund' 
      end
    end
    resources :logs
    resources :monitor
  end

  resources :pages do
    collection do
      get 'not_found' 
      get 'whoops'
    end
    member { post :mercury_update }
  end

  resources :vouchers do
    collection do
      post 'redeem'
    end
    member do
      get 'redeem_now'
    end
  end
  
  resources :deals do
    collection do
      get 'my_deals'
    end
    member do
      get 'usage'
      get 'vouchers'      
      get 'log'      
      get 'payments'
      get 'publish'
      get 'banner'
      get 'example_iframe'
      get 'iframe', constraints: lambda { |r| 
        r.subdomain.present? && r.subdomain != 'www' && r.subdomain != 'staging'
      }
      get 'facebook'
    end
  end
  
  resources :company do  
    member do
      get 'settle'     
      get 'masquerade'     
      get 'seller_payments'
      post 'settle'
    end
    collection do

      post 'redeem_voucher'
    end
  end

  # devise_for :users
  devise_for :users, :controllers => { 
    :sessions => "sessions",
    :registrations => "registrations"
  }
  
  resources :payments

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  root :to => "home#index"
  
  # get 404s...
  # match '*a', :to => 'pages#not_found'  
  

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														