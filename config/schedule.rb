# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# env "PATH", ENV["PATH"]
# env "GEM_HOME", ENV["GEM_HOME"]
# set :job_template, "bash -l -c 'rvm 1.9.3 && :job'"
# job_type :rake, "rvm 1.9.3 && cd :path && RAILS_ENV=:environment bundle exec rake :task --silent :output"

# every 2.minutes do
#   rake "payments"
# end

every 1.minute do
  rake "send_log_alerts"
end

every 1.week do
  rake "truncate_logs"
end

every :reboot do
  command "sudo bluepill load /home/ubuntu/#{@environment}/config/#{@environment}.pill"
end

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
