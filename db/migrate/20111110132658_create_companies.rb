class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :website
      t.string :subdomain
      t.string :facebook
      t.string :twitter
      t.text :description
      t.string :address_line1
      t.string :address_line2
      t.string :post_code
      t.string :phone
      t.string :city

      t.timestamps
    end
  end
end
