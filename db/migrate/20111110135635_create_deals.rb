class CreateDeals < ActiveRecord::Migration
  def change
    create_table :deals do |t|
      t.integer :company
      t.string :title
      t.text :description
      t.integer :quantity
      t.integer :min_quantity
      t.datetime :start_time
      t.datetime :end_time
      t.decimal :price, :precision => 11, :scale => 2
      t.decimal :shipping_cost, :precision => 11, :scale => 2
      t.boolean :public
      t.decimal :lat
      t.decimal :lng

      t.timestamps
    end
  end
end
