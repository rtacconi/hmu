class RenameCountryToCity < ActiveRecord::Migration
  def up
    # AO 09-12-11 - seems to have been done in an earlier migration?
    # rename_column :companies, :country, :city
  end

  def down
    # rename_column :companies, :city, :country
  end
end
