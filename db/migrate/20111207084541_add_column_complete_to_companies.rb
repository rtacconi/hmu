class AddColumnCompleteToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :completed, :boolean, :default => false
  end
end
