class AddPermanentToPages < ActiveRecord::Migration
  def change
    add_column :pages, :permanent, :boolean
  end
end
