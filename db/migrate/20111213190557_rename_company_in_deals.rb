class RenameCompanyInDeals < ActiveRecord::Migration
  def up
    rename_column :deals, :company, :company_id
  end

  def down
    rename_column :deals, :company_id, :company
  end
end
