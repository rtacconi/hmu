class AddToDeals < ActiveRecord::Migration
  def change
    add_column :deals, :small_print, :string
    add_column :deals, :show_map, :boolean
    add_column :deals, :post_code, :string
    add_column :deals, :rrp, :decimal, :precision => 11, :scale => 2
  end
end
