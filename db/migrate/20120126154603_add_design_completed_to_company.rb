class AddDesignCompletedToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :design_completed, :boolean, :default => false
  end
end
