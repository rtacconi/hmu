class AddAttachmentBackgroundToCompany < ActiveRecord::Migration
  def self.up
    add_column :companies, :background_file_name, :string
    add_column :companies, :background_content_type, :string
    add_column :companies, :background_file_size, :integer
    add_column :companies, :background_updated_at, :datetime
    add_column :companies, :font_colour, :string, :default => '000000'
  end

  def self.down
    remove_column :companies, :background_file_name
    remove_column :companies, :background_content_type
    remove_column :companies, :background_file_size
    remove_column :companies, :background_updated_at
    remove_column :companies, :font_colour, :string
  end
end
