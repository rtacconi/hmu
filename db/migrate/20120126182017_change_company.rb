class ChangeCompany < ActiveRecord::Migration
  def change
    remove_column :companies, :design_completed
    remove_column :companies, :completed
    add_column :companies, :tile_background, :boolean, :default => false
  end
end
