class AddAcceccTokenToUser < ActiveRecord::Migration
  def change
    add_column :users, :access_token, :string
    add_column :users, :access_token_expires, :string
  end
end
