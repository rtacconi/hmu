class ChangeDealPublished < ActiveRecord::Migration
  def change
    remove_column :deals, :public
    remove_column :deals, :published
    add_column :deals, :published, :boolean, :default => true
  end
end
