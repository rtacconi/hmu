class AddPostedTimesToDeals < ActiveRecord::Migration
  def change
    add_column :deals, :posted_times, :integer, :default => 0
  end
end
