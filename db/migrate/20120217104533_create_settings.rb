class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :bank_name
      t.string :branch
      t.string :sort_code
      t.string :account_number

      t.timestamps
    end
  end
end
