class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :gateway
      t.integer :transaction_id
      t.datetime :transaction_date
      t.decimal :amount, :precision => 11, :scale => 2
      t.string :status
      t.boolean :fetched
      t.datetime :fetched_time

      t.timestamps
    end
  end
end
