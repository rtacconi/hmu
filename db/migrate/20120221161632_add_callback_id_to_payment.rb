class AddCallbackIdToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :callback_id, :integer
    add_column :payments, :user_id, :integer
    add_column :payments, :email, :string
  end
end
