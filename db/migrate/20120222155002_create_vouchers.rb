class CreateVouchers < ActiveRecord::Migration
  def change
    create_table :vouchers do |t|
      t.references :deal
      t.references :user
      t.string :code
      t.boolean :redeemed, :default => false
      t.timestamps
    end
  end
end
