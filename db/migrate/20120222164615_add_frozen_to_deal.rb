class AddFrozenToDeal < ActiveRecord::Migration
  def change
    add_column :deals, :frozen, :boolean, :default => false
  end
end
