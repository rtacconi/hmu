class AddPaymentIdToVoucher < ActiveRecord::Migration
  def change
    add_column :vouchers, :payment_id, :integer
  end
end
