class ChangeFetchedInPayments < ActiveRecord::Migration
  def change
    rename_column :payments, :fetched, :processed
  end
end
