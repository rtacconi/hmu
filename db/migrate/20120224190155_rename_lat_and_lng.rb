class RenameLatAndLng < ActiveRecord::Migration
  def change
    remove_column :deals, :lat
    remove_column :deals, :lng
    add_column :deals, :latitude, :float
    add_column :deals, :longitude, :float
  end
end
