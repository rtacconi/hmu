class CreateVoucherCodes < ActiveRecord::Migration
  def change
    create_table :voucher_codes do |t|
      t.integer :deal_id
      t.string :code
      t.boolean :used, :dafault => false

      t.timestamps
    end
  end
end
