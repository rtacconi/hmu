class AddExpiryTimeToDeal < ActiveRecord::Migration
  def change
    add_column :deals, :expiry_time, :datetime
  end
end
