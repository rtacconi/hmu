class DropVouchersAddPaymentIdTovouchersCode < ActiveRecord::Migration
  def change
    drop_table :vouchers
    add_column :voucher_codes, :payment_id, :integer
  end
end
