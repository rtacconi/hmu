class ChangeTransationIdInPayment < ActiveRecord::Migration
  def change
    remove_column :payments, :transaction_id
    add_column :payments, :transaction_id, :string
  end
end
