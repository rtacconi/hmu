class AddPublishOnFacebookToDeal < ActiveRecord::Migration
  def change
    add_column :deals, :publish_on_facebook, :boolean
  end
end
