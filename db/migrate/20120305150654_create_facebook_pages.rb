class CreateFacebookPages < ActiveRecord::Migration
  def change
    create_table :facebook_pages do |t|
      t.integer :fb_page_id, :limit => 8
      t.integer :company_id

      t.timestamps
    end
  end
end
