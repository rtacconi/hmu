class AddNotTippedSentToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :not_tipped_sent, :boolean, :default => false
  end
end
