class ChangeSmallPrintColumn < ActiveRecord::Migration
  def up
    remove_column :deals, :small_print
    add_column :deals, :small_print, :text
  end

  def down
    remove_column :deals, :small_print
    add_column :deals, :small_print, :string
  end
end
