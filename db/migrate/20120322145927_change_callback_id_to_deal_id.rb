class ChangeCallbackIdToDealId < ActiveRecord::Migration
  def change
    rename_column :payments, :callback_id, :deal_id
  end
end
