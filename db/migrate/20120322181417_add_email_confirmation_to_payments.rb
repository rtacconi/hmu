class AddEmailConfirmationToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :email_confirmation, :string
  end
end
