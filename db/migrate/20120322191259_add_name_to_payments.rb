class AddNameToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :first_name, :string
    add_column :payments, :last_name, :string
  end
end
