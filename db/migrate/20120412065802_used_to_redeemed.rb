class UsedToRedeemed < ActiveRecord::Migration
  def up
    remove_column :voucher_codes, :used
    add_column :voucher_codes, :redeemed, :boolean, :null => false, :default => false
  end

  def down
    remove_column :voucher_codes, :redeemed
    add_column :voucher_codes, :used, :boolean, :default => false
  end
end
