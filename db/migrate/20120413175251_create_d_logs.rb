class CreateDLogs < ActiveRecord::Migration
  def change
    create_table :d_logs do |t|
      t.string :level
      t.text :message
      t.references :deal
      t.references :payment
      t.references :user
      t.timestamps
    end
  end
end
