class SetAllVouchersToFalse < ActiveRecord::Migration
  def up
    remove_column :voucher_codes, :redeemed
    add_column :voucher_codes, :redeemed, :boolean, :null => false, :default => false
    Payment.update_all({:processed => false, :status => :pending})
  end

  def down
  end
end
