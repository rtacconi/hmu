class ResetPaymentsAndVouchers < ActiveRecord::Migration
  def up
    DLog.info "Resetting all payments and vouchers..."
    Payment.update_all({:processed => false, :status => :pending})
    VoucherCode.update_all({:payment_id => nil, :redeemed => false})
    Deal.update_all({:frozen => false})
  end

  def down
  end
end
