class AddFieldsToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :address_line1, :string
    add_column :payments, :post_code, :string
    add_column :payments, :city, :string
    add_column :payments, :country, :string
  end
end
