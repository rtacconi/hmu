class PaymentsChangeFields < ActiveRecord::Migration
  def change
    rename_column :payments, :address_line1, :billing_address_line1
  end
end
