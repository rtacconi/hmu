class RemoveSubdomains < ActiveRecord::Migration
  def up 
    remove_column :companies, :subdomain
  end

  def down
  end
end
