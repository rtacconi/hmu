class CreateSellerPayments < ActiveRecord::Migration
  def change
    create_table :seller_payments do |t|
      t.references :company
      t.decimal :amount, :precision => 11, :scale => 2
      t.string :bank_name
      t.string :branch
      t.string :sort_code
      t.string :account_number
      t.references :user
      t.timestamps
    end
  end
end
