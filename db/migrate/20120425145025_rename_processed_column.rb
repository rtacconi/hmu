class RenameProcessedColumn < ActiveRecord::Migration
  def up
    rename_column :payments, :processed, :email_result_sent
  end

  def down
  end
end
