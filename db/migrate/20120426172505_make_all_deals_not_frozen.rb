class MakeAllDealsNotFrozen < ActiveRecord::Migration
  def up
    Deal.update_all({:frozen => false})
  end

  def down
  end
end
