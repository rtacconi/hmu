class NewLoggingColumns < ActiveRecord::Migration
  def up
    add_column :d_logs, :notified, :boolean, :null => false, :default => false
  end

  def down
  end
end
