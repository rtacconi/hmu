class MoreLoggingColumns < ActiveRecord::Migration
  def up 
    add_column :d_logs, :ip, :string
    add_column :d_logs, :url, :string
  end

  def down
  end
end
