class TippedAtColumn < ActiveRecord::Migration
  def up
    add_column :deals, :tipped_at, :datetime
  end

  def down
  end
end
