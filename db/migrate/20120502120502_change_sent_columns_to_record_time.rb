class ChangeSentColumnsToRecordTime < ActiveRecord::Migration
  def up 
    # temporarily rename old column as our new column also has samename for not tipped
    rename_column :payments, :not_tipped_sent, :not_tipped_sent_old
    
    #new columns
    add_column :payments, :voucher_sent, :datetime
    add_column :payments, :payment_failed_sent, :datetime
    add_column :payments, :not_tipped_sent, :datetime
    add_column :payments, :deal_never_tipped_sent, :datetime
    add_column :payments, :refunded_sent, :datetime
    
    # now copy over old data
    Payment.all.each do |p|
      DLog.info "Migrating to new email sent columns", {:payment => p}
      p.voucher_sent = p.updated_at if p.status == :paid && p.email_result_sent
      p.payment_failed_sent = p.updated_at if p.status == :failed && p.email_result_sent
      p.not_tipped_sent = p.updated_at if p.not_tipped_sent_old
      # get rid of old payments that are no longer valid i.e. if they're missing 
      # details that avoid them being saved.
      if !p.save
        DLog.info "Deleting, payment is invalid and probably from old gateway", {:payment => p}
        p.destroy
      end
    end
    
    #remove old columns
    remove_column :payments, :not_tipped_sent_old
    remove_column :payments, :email_result_sent
    
    # remove these unused columns
    remove_column :payments, :transaction_id
    remove_column :payments, :card_type
    remove_column :payments, :fetched_time
    remove_column :payments, :gateway
    
  end

  def down
  end
end
