class OrderConfSentCol < ActiveRecord::Migration
  def up
    
    add_column :payments, :order_confirmation_sent, :datetime
    
    # now copy over old data
    Payment.all.each do |p|
      DLog.info "Migrating to new email order conf column", {:payment => p}
      p.order_confirmation_sent = p.created_at
      p.save!
    end
  end

  def down
  end
end
