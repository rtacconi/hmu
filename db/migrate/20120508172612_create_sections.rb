class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.string :name

      t.timestamps
    end
    
    add_column :pages, :section_id, :integer
    
    Section.create!(:name => 'blog')
  end
end
