class AddVoucherExpiryDate < ActiveRecord::Migration
  def up
    add_column :deals, :voucher_expiry, :datetime
  end

  def down
    # this should not be left empty! Use change
  end
end
