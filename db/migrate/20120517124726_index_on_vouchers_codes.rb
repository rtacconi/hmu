class IndexOnVouchersCodes < ActiveRecord::Migration
  def up
    add_index :voucher_codes, [:code]
  end

  def down
  end
end
