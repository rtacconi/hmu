class RenameFrozen < ActiveRecord::Migration
  def change
    rename_column :deals, :frozen, :blocked
  end
end
