class RemoveVoucherExpiry < ActiveRecord::Migration
  def change
    remove_column :deals, :voucher_expiry
  end
end
