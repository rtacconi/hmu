class AddConfirmationStateToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :confirmation_state, :string
  end
end
