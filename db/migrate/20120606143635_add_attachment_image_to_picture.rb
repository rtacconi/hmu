class AddAttachmentImageToPicture < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :photo_file_name
      t.string :photo_content_type
      t.integer :photo_file_size
      t.datetime :photo_updated_at
      t.integer :imageable_id
      t.string :imageable_type
      t.string :link_code
    end
    
    add_column :deals, :link_code, :string
  end
end
