# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120606143635) do

  create_table "admin_analytics", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "admin_moderations", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.string   "website"
    t.string   "facebook"
    t.string   "twitter"
    t.text     "description"
    t.string   "address_line1"
    t.string   "address_line2"
    t.string   "post_code"
    t.string   "phone"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "your_name"
    t.integer  "company_id"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.string   "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.string   "font_colour",             :default => "970310"
    t.boolean  "tile_background",         :default => false
    t.string   "deal_logo_file_name"
    t.string   "deal_logo_content_type"
    t.integer  "deal_logo_file_size"
    t.datetime "deal_logo_updated_at"
    t.string   "confirmation_state"
  end

  create_table "d_logs", :force => true do |t|
    t.string   "level"
    t.text     "message"
    t.integer  "deal_id"
    t.integer  "payment_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "notified",   :default => false, :null => false
    t.string   "ip"
    t.string   "url"
  end

  create_table "deals", :force => true do |t|
    t.integer  "company_id"
    t.string   "title"
    t.text     "description"
    t.integer  "quantity"
    t.integer  "min_quantity"
    t.datetime "start_time"
    t.datetime "end_time"
    t.decimal  "price",               :precision => 11, :scale => 2
    t.decimal  "shipping_cost",       :precision => 11, :scale => 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "show_map"
    t.string   "post_code"
    t.decimal  "rrp",                 :precision => 11, :scale => 2
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "published",                                          :default => true
    t.integer  "posted_times",                                       :default => 0
    t.text     "how_to_redeem"
    t.boolean  "blocked"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "street"
    t.datetime "expiry_time"
    t.boolean  "publish_on_facebook"
    t.text     "small_print"
    t.datetime "tipped_at"
    t.string   "link_code"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "facebook_pages", :force => true do |t|
    t.integer  "fb_page_id", :limit => 8
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "images", :force => true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "impressions", :force => true do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "impressions", ["controller_name", "action_name", "ip_address"], :name => "controlleraction_ip_index"
  add_index "impressions", ["controller_name", "action_name", "request_hash"], :name => "controlleraction_request_index"
  add_index "impressions", ["controller_name", "action_name", "session_hash"], :name => "controlleraction_session_index"
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], :name => "poly_ip_index"
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], :name => "poly_request_index"
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], :name => "poly_session_index"
  add_index "impressions", ["user_id"], :name => "index_impressions_on_user_id"

  create_table "mercury_images", :force => true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pages", :force => true do |t|
    t.string   "title"
    t.string   "summary"
    t.text     "body"
    t.boolean  "published"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "permanent"
    t.integer  "section_id"
  end

  create_table "payments", :force => true do |t|
    t.datetime "transaction_date"
    t.decimal  "amount",                  :precision => 11, :scale => 2
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "deal_id"
    t.integer  "user_id"
    t.string   "email"
    t.string   "token"
    t.string   "email_confirmation"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "billing_address_line1"
    t.string   "post_code"
    t.string   "city"
    t.string   "country"
    t.datetime "voucher_sent"
    t.datetime "payment_failed_sent"
    t.datetime "not_tipped_sent"
    t.datetime "deal_never_tipped_sent"
    t.datetime "refunded_sent"
    t.datetime "order_confirmation_sent"
  end

  create_table "pictures", :force => true do |t|
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.string   "link_code"
  end

  create_table "sections", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seller_payments", :force => true do |t|
    t.integer  "company_id"
    t.decimal  "amount",         :precision => 11, :scale => 2
    t.string   "bank_name"
    t.string   "branch"
    t.string   "sort_code"
    t.string   "account_number"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "settings", :force => true do |t|
    t.string   "bank_name"
    t.string   "branch"
    t.string   "sort_code"
    t.string   "account_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin"
    t.integer  "company_id"
    t.string   "access_token"
    t.string   "access_token_expires"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "voucher_codes", :force => true do |t|
    t.integer  "deal_id"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "payment_id"
    t.boolean  "redeemed",   :default => false, :null => false
  end

  add_index "voucher_codes", ["code"], :name => "index_voucher_codes_on_code"

end
