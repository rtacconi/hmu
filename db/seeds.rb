# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
admin =   User.create(:email => "admin@gmail.com", 
                      :password => "charli3", 
                      :password_confirmation => "charli3",
                      :admin => true)
user1 =   User.create(:email => "user1@gmail.com", 
                      :password => "password", 
                      :password_confirmation => "password")
            
wema =   Company.create(:name => 'We Make Websites Ltd',
                         :website => 'wemakewebsites.com',
                         :subdomain => 'wemakewebsites',
                         :twitter => 'wemakewebsites',
                         :facebook => 'wemakewebsites',
                         :description => 'Web dev company',
                         :address_line1 => 'Studio 5',
                         :address_line1 => '155 Commercial St',
                         :post_code => 'E1 6BJ',
                         :phone => '07752 715590',
                         :city => 'London')
               
acme = Company.create(:name => 'We Make Websites Ltd',
                      :website => 'wemakewebsites.com',
                      :subdomain => 'wemakewebsites',
                      :twitter => 'wemakewebsites',
                      :facebook => 'wemakewebsites',
                      :description => 'Web dev company',
                      :address_line1 => 'Studio 5',
                      :address_line1 => '155 Commercial St',
                      :post_code => 'E1 6BJ',
                      :phone => '07752 715590',
                      :city => 'London')
                      
admin.update_attribute(:company, wema)
user1.update_attribute(:company, acme)

Page.create!(:title => 'Terms & Conditions', 
             :summary => 'Please read carefully',
             :body => 'This web site...',
             :published => true,
             :permanent => true)
             
Page.create!(:title => 'Privacy Policy', 
            :summary => 'How your privacy is protected',
            :body => 'We do not give your information to any...',
            :published => true,
            :permanent => true)

Page.create!(
  title: "Welcome",
  summary: "Welcome to this blog",
  body: "This is the body of the first post",
  published: true,
  permanent: false,
  section: Section.last)
