class ToHaml
  def initialize(path)
    @path = path
  end
  
  def convert!
    Dir["#{@path}/**/*.erb"].each do |file|
      `html2haml -rx #{file} #{file.gsub(/\.erb$/, '.haml')}`
    end
    self
  end
  
  def move!
    Dir["#{@path}/**/*.erb"].each do |file|
      `mv #{file} #{@path}/old/`
    end
    self
  end
end

path = File.join(File.dirname(__FILE__), 'app', 'views')
ToHaml.new(path).convert!.move!