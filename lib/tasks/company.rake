namespace "company" do
  desc "Setting confirmation_state for all users to details_completed. Use this task only once."
  task "set_confirmation_state" => :environment do
    puts "Affected rows: " + Company.update_all(:confirmation_state => "details_completed").to_s
  end
end