desc "process payments"
task :send_log_alerts => :environment do
  DLog.send_alerts("Error")
  DLog.send_alerts("Warning")
end

task :truncate_logs => :environment do
  DLog.truncate_to(10000)
end