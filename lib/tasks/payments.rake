desc "process payments"
task :payments => :environment do
  Payment.process(Deal.not_blocked)
end
