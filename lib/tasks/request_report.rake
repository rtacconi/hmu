desc "just requests"
task :requests_report do
  `bundle exec rspec spec/requests/ --format html > spec/requests_report.html`
end

desc "all rspec"
task :rspec_report do
  `bundle exec rspec spec/ --format html > spec/rspec_report.html`
end