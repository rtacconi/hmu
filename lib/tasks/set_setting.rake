desc "set setting for companies"
task :set_setting => :environment do
  companies = Company.all
  companies.each do |c|
    puts "updating company #{c.name}"
    c.update_attribute(:setting, Setting.create!)
  end
end