require 'spec_helper'

describe Admin::MonitorController do

  describe "GET 'index'" do
    before(:each) do
      log_in_as_admin
    end
    
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

end
