require 'spec_helper'

describe Admin::UsersController do
  describe "GET 'index'" do
    it "returns http success if the user is an admin" do
      sign_in User.make!(:admin => true)
      get 'index'
      response.should be_success
    end
    
    it "returns redirect if the user is not admin" do
      sign_in User.make!(:admin => false)
      get 'index'
      response.should redirect_to root_path
    end
    
    it "loads all users" do
      sign_in User.make!(:admin => false)
      get 'index'
      assigns(:users)
    end
  end
end
