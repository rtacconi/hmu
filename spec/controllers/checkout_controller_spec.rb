require 'spec_helper'

describe CheckoutController do
  describe "GET confirm" do
    def valid_params
      {"resource_id"=>"0162CKWGAJ", "resource_type"=>"bill", "resource_uri"=>"https://sandbox.gocardless.com/api/v1/bills/0162CKWGAJ", "signature"=>"f07ca39529e68c6b9dbb219e448e2dd12ff19abe242d446bf3b3a0c7218404a4", "controller"=>"checkout", "action"=>"confirm", 'state' => 2}
    end
    
    def do_confirm
      resource = mock(GoCardless::Resource, :amount => '12.44', 
                                            :created_at => 10.minutes.ago, 
                                            :user_id => 1)
      GoCardless.stub!(:confirm_resource).and_return(resource)
      payment = Payment.stub!(:new).and_return(mock_model(Payment))
      user = double('GoCardless::User')
      user.stub!(:email => 'jd@ex.com')
      user = GoCardless.stub_chain(:client, :user).and_return(user)
      payment.should_receive(:email).with(user.email)
      payment.stub(:save!)
    end
    
    it "should render confirm" do
      pending
      do_confirm
      get :confirm, valid_params
      response.should render_template 'confirm'
    end
    
    it "should create a new payment record" do
      pending
      resource = mock(GoCardless::Resource, :amount => '12.44', 
                                            :created_at => 10.minutes.ago, 
                                            :user_id => 1)
      GoCardless.stub!(:confirm_resource).and_return(resource)
      user = GoCardless.stub_chain(:client, :user).and_return(user)
      payment = Payment.stub!(:new).and_return(mock_model(Payment, :amount => '12.44', 
                                            :created_at => 10.minutes.ago, 
                                            :user_id => 1))
      payment.amount.should == '12.44'
      get :confirm, valid_params
      Payment.count.should > 0
    end
  end
end