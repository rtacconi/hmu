require 'spec_helper'

describe CompanyController do
  def valid_profile_attributes
    {:name => 'Acme Inc', 
     :your_name => 'John Doe', 
     :phone => '01 2333333', 
     :post_code => 'EC2 WEE',
     :address_line1 => '34 John Street',
     :logo => get_image}
  end
  
  describe "PUT update" do
    describe "with valid params" do
      it "redirects to desgin if profile is completed" do
        pending
        company = Company.create! valid_profile_attributes
        put :update, :id => company.id, :company => {:font_colour => '#fff'}
        response.should redirect_to(design_path)
      end
    end
    
    describe "with invalid params" do
      it "should redirect to profile" do
        pending
        company = Company.create! valid_profile_attributes
        put :update, :id => company.id, :company => {:name => ''}
        response.should render_template '/profile/index'
      end
    end
  end
  
end
