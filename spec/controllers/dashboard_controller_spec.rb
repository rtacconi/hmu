require 'spec_helper'

describe DashboardController do
  include Devise::TestHelpers
  
  describe "GET 'index'" do
    context "as guest" do
      it "should redirect to sign in" do
        get 'index'
        response.should redirect_to new_user_session_url
      end
    end
    
    context "as logged user" do
      it "should load past and current deals" do
        @user = User.make!
        log_in @user
        Deal.should_receive(:past).with(@user.company)
        Deal.should_receive(:current).with(@user.company)
        get 'index'
        assigns(:company).should_not be_nil
      end
    end
  end
end