require 'spec_helper'

describe DealsController do

  def valid_attributes
    {:title => 'Bunch of something',
     :description => 'We are offering...',
     :small_print => 'Do not...',
     :price => 12.10,
     :rrp => 20.00,
     :start_time => 10.seconds.since,
     :end_time => 2.days.since,
     :quantity => 20,
     :post_code => 'EC2 3EE',
     :min_quantity => 10}
  end
  
  def create_objects
    # @company = Company.make!
    # @user = User.make!(:company => @company)
    # @deal = Deal.make!(:company => @company)
    @user = User.make!
    @deal = Deal.make! :company => @user.company
    @company = @user.company
  end

  it 'test @deal' do
    create_objects
    @deal.should be_present
    @company.should be_present
    @user.should be_present
  end
  
  describe "GET index" do
    context "as admin" do
      it "renders index" do
        log_in_as_admin
        Deal.should_receive(:order).and_return(mock_model(Deal).as_null_object)
        get :index
        response.should render_template('index')
      end
    end
    
    context "as normal user" do
      it "should redirect_to root path" do
        create_objects
        sign_in User.make!(:admin => false, :company => @company)
        get :index
        response.should redirect_to root_path
      end      
    end
  end

  describe "GET show" do
    it "assigns the requested deal as @deal" do
      create_objects
      get :show, :id => @deal.id
      assigns(:deal).should eq(@deal)
    end
  end

  describe "GET new" do
    it "assigns a new deal as @deal" do
      create_objects
      log_in_as_admin
      get :new
      assigns(:deal).should be_a_new(Deal)
    end
  end

  describe "GET edit" do
    it "assigns the requested deal as @deal" do
      create_objects
      sign_in @user
      get :edit, :id => @deal.id
      assigns(:deal).should eq(@deal)
    end
  end

  context "as logged in user" do
    before(:each) do
      create_objects
      @user = User.make!(:admin => false, :company => @company)
      log_in @user
    end
        
    describe "POST create" do      
      describe "with valid params" do
        it "creates a new Deal" do
          expect {
            post :create, :deal => valid_attributes
          }.to change(Deal, :count).by(1)
        end

        it "assigns a newly created deal as @deal" do
          post :create, :deal => valid_attributes
          assigns(:deal).should be_a(Deal)
          assigns(:deal).should be_persisted
        end

        it "redirects to the created deal" do
          post :create, :deal => valid_attributes
          response.should redirect_to deal_path(Deal.last)
        end
      
        it "should set the company id" do
          post :create, :deal => valid_attributes
          assigns(:deal).company.should_not be_nil
        end
      end
      
      describe "DELETE destroy" do
        it "destroys the requested deal" do
          deal = Deal.new valid_attributes
          deal.company = @company
          deal.save
          expect {
            delete :destroy, :id => deal.id
          }.to change(Deal, :count).by(-1)
        end

        it "redirects to the deals list" do
          deal = Deal.new valid_attributes
          deal.company = @company
          deal.save
          delete :destroy, :id => deal.id
          response.should redirect_to(deals_url)
        end
      end
    end

    describe "PUT update" do
      pending
      describe "with valid params" do
        it "updates the requested deal" do
          deal = Deal.new valid_attributes
          deal.company = @company
          deal.save
          # Assuming there are no other deals in the database, this
          # specifies that the Deal created on the previous line
          # receives the :update_attributes message with whatever params are
          # submitted in the request.
          Deal.any_instance.should_receive(:update_attributes).with({"title" => 'A Deal'})
          put :update, :id => deal.id, :deal => {"title" => 'A Deal'}
        end

        it "assigns the requested deal as @deal" do
          deal = Deal.new valid_attributes
          deal.company = @company
          deal.save
          put :update, :id => deal.id, :deal => valid_attributes
          assigns(:deal).should eq(deal)
        end

        it "redirects to the deal" do
          deal = Deal.new valid_attributes
          deal.company = @company
          deal.save
          put :update, :id => deal.id, :deal => valid_attributes
          response.should redirect_to(deal)
        end
      end

      describe "with invalid params" do
        it "assigns the deal as @deal" do
          deal = Deal.new valid_attributes
          deal.company = @company
          deal.save
          # Trigger the behavior that occurs when invalid params are submitted
          Deal.any_instance.stub(:save).and_return(false)
          put :update, :id => deal.id, :deal => {}
          assigns(:deal).should eq(deal)
        end

        it "re-renders the 'edit' template" do
          deal = Deal.new valid_attributes
          deal.company = @company
          deal.save
          # Trigger the behavior that occurs when invalid params are submitted
          Deal.any_instance.stub(:save).and_return(false)
          put :update, :id => deal.id, :deal => {}
          response.should render_template("edit")
        end
      end
    end
    
    describe "GET my_deals" do
      it "should render my_deals template" do
        get :my_deals
        
        response.should render_template 'my_deals'
      end
      
      it "should load past and current deals" do
        Deal.should_receive(:past)
        Deal.should_receive(:current)
        get 'my_deals'
        assigns(:company).should_not be_nil
      end
    end
  end
  
  describe "GET usage" do
    it "should be a success" do
      pending
      create_objects
      get :usage, :id => @deal.id
      response.should be_success
    end
  end
end
