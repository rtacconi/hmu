require 'spec_helper'

describe FacebookController do
  
  describe "GET index" do
    it "should render" do
      get :index
      response.should render_template 'index'
    end
  end

  describe "POST 'create'" do
    it "returns http redirect" do
      pending
      User.delete_all
      Company.delete_all
      Deal.delete_all
      FakeWeb.register_uri(:get, "hhttps://graph.facebook.com/oauth/authorize?client_id=237756859639775&display=popup&scope=publish_stream%2Coffline_access%2Cmanage_pages%2Coffline_access&redirect_uri=http%3A%2F%2Flvh.me%3A3000%2Ffacebook_callback%2F1", :body => '')
      @user = User.make! :company => Company.make!
      log_in @user
      @deal = Deal.make! :company => @user.company
      post 'create', :id => @deal.id
      response.should redirect_to @deal
    end
  end

end
