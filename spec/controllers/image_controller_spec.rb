require 'spec_helper'

describe ImageController do

  describe "GET 'create'" do
    it "returns http success" do
      @user = User.make!
      sign_in @user
      post 'create', :format => :js
      response.should be_success
    end
  end

end
