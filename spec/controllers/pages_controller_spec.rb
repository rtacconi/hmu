require 'spec_helper'

describe PagesController do
  def valid_attributes
    {:published => true}
  end
  
  def persistent_page_attributes
    {:published => true, :permanent => true}
  end
  
  context "normal user" do
    before(:each) do
      @commpany = mock_model(Company, :id => 1, :complete => false)
      @user = mock_model(User, :id => 100, :company => 1, :admin => false).as_null_object
      log_in @user  
      create_pages
    end
    
    it "redirects to root_url with :index" do
      page = Page.create! valid_attributes
      get :index
      redirect_to root_url
    end
    
    it "redirects to root_url" do
      page = Page.create! valid_attributes
      get :edit, :id => page.id
      redirect_to root_url
    end
    
    it "redirects to root_url with :new" do
      page = Page.create! valid_attributes
      get :new
      redirect_to root_url
    end
    
    
    it "redirects to the page with :create" do
      post :create, :page => valid_attributes
      response.should redirect_to page_url(Page.last)
    end
    
    it "redirects to the page with :update" do
      page = Page.create! valid_attributes
      put :update, :id => page.id, :page => valid_attributes
      response.should redirect_to pages_path
    end
    
    it "redirects with :destroy" do
      page = Page.create! valid_attributes
      delete :destroy, :id => page.id
      response.should redirect_to pages_url
    end
  end
  
  describe "GET show" do
    it "assigns the requested page as @page" do
      page = Page.create! valid_attributes
      get :show, :id => page.id
      assigns(:page).should eq(page)
    end
    
    it "redirects to / if unpublished" do
      page = Page.create! :published => false
      get :show, :id => page.id
      response.should redirect_to root_url
    end
    
    it "renders the file under static directory if page is static" do
      page = Page.create! :id => 4
      get :show, :id => page.id
      response.should render_template page.id
    end
  end
  
  context "as admin" do
    before(:each) do
      log_in_as_admin
      Page.delete_all
      create_pages
    end
    
    describe "GET index" do      
      it "assigns all pages as @pages" do      
        page = Page.create! valid_attributes
        get :index
        assigns(:pages).should eq(Page.all)
      end
    end

    describe "GET new" do
      it "assigns a new page as @page" do
        get :new
        assigns(:page).should be_an_instance_of(Page)
      end
    end

    describe "GET edit" do
      it "assigns the requested page as @page" do
        page = Page.create! valid_attributes
        get :edit, :id => page.id
        assigns(:page).should eq(page)
      end
    end

    describe "POST create" do
      describe "with valid params" do
        it "creates a new Page" do
          expect {
            post :create, :page => valid_attributes
          }.to change(Page, :count).by(1)
        end

        it "assigns a newly created page as @page" do
          post :create, :page => valid_attributes
          assigns(:page).should be_a(Page)
          assigns(:page).should be_persisted
        end

        it "redirects to the created page" do
          post :create, :page => valid_attributes
          response.should redirect_to(Page.last)
        end
      end

      describe "with invalid params" do
        it "assigns a newly created but unsaved page as @page" do
          # Trigger the behavior that occurs when invalid params are submitted
          Page.any_instance.stub(:save).and_return(false)
          post :create, :page => {}
          assigns(:page).should be_a_new(Page)
        end

        it "re-renders the 'new' template" do
          # Trigger the behavior that occurs when invalid params are submitted
          Page.any_instance.stub(:save).and_return(false)
          post :create, :page => {}
          response.should render_template("new")
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        it "updates the requested page" do
          page = Page.create! valid_attributes
          # Assuming there are no other pages in the database, this
          # specifies that the Page created on the previous line
          # receives the :update_attributes message with whatever params are
          # submitted in the request.
          Page.any_instance.should_receive(:update_attributes).with({'these' => 'params'})
          put :update, :id => page.id, :page => {'these' => 'params'}
        end

        it "assigns the requested page as @page" do
          page = Page.create! valid_attributes
          put :update, :id => page.id, :page => valid_attributes
          assigns(:page).should eq(page)
        end

        it "redirects to the page" do
          page = Page.create! valid_attributes
          put :update, :id => page.id, :page => valid_attributes
          response.should redirect_to pages_path
        end
      end

      describe "with invalid params" do
        it "assigns the page as @page" do
          page = Page.create! valid_attributes
          # Trigger the behavior that occurs when invalid params are submitted
          Page.any_instance.stub(:save).and_return(false)
          put :update, :id => page.id, :page => {}
          assigns(:page).should eq(page)
        end

        it "re-renders the 'edit' template" do
          page = Page.create! valid_attributes
          # Trigger the behavior that occurs when invalid params are submitted
          Page.any_instance.stub(:save).and_return(false)
          put :update, :id => page.id, :page => {}
          response.should render_template("edit")
        end
      end
      
        context "when the page is not persistent" do
          describe "DELETE destroy" do
            it "destroys the requested page" do
              page = Page.create! valid_attributes
              expect {
                delete :destroy, :id => page.id
              }.to change(Page, :count).by(-1)
            end

            it "redirects to the pages list" do
              page = Page.create! valid_attributes
              delete :destroy, :id => page.id
              response.should redirect_to(pages_url)
            end
          end
        end
        
        context "when the page is not persistent" do
          describe "DELETE destroy" do
            it "do not destroys the requested page" do
              page = Page.create! persistent_page_attributes
              expect {
                delete :destroy, :id => page.id
              }.to change(Page, :count).by(0)
            end

            it "redirects to the pages list" do
              page = Page.create! persistent_page_attributes
              delete :destroy, :id => page.id
              response.should redirect_to(pages_url)
            end
          end
        end
      end
    end
    
    describe "GET not_found" do
      it "should display not found page" do
        get :not_found
        response.should be_success
      end
    end
end
