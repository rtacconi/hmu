require 'spec_helper'

describe PaymentsController do
  describe "POST create" do
    it "should create a new payment with matching emails" do
      pending
      @payment = Payment.stub(:new).and_return(mock_model(Payment, :id => nil))
      @payment.should_receive(:save).and_return(true)
      post :create, :payment => {
        :email => 'buyer@example.com', 
        :email_confirmation => 'buyer@example.com'
      }
    end
    
    it "should not create a new payment with different emails" do
      pending
      @payment = Payment.stub(:new).and_return(mock_model(Payment, :id => nil))
      @payment.should_receive(:save).and_return(true)
      post :create, :payment => {
        :email => 'buyer@example.com', 
        :email_confirmation => 'buyer2@example.com'
      }
    end
  end
end
