require 'spec_helper'

describe ProfileController do
  before(:each) do
    log_in_as_user
  end

  describe "GET index" do
    it "returns http success" do
      get :index
      response.should be_success
    end
  end

end
