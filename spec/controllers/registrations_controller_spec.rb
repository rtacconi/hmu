require 'spec_helper'

describe RegistrationsController do
  describe "POST create" do
    def valid_attributes
      {:user => {
        :company_attributes => {
          "name"=>"Bungee limited", 
          "your_name"=>"Richard Bass", 
          "phone"=>"01 34234444"
        },
        "email"=>"rt@example.com",
        "password"=>"password",
        "password_confirmation"=>"password"
      }}
    end
    
    # it is better to test if deliver is called but I was getting an error
    # this workaround tests the templates rendered by the notifier
    # https://github.com/plataformatec/devise/wiki/How-To:-Controllers-and-Views-tests-with-Rails-3-(and-rspec)
    
    it 'notifies Charlie that a new user has been created' do
      @request.env["devise.mapping"] = Devise.mappings[:user] # used to avoid the routing error
      post :create, valid_attributes
      response.should render_template 'notifier/new_user'
    end
    
    it 'sends a welcome message to the new seller' do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :create, valid_attributes
      response.should render_template 'notifier/welcome'
    end
  end
end
