require 'spec_helper'

describe SessionsController do
  tests Devise::SessionsController
  include Devise::TestHelpers

  it "#create doesn't raise exception after Warden authentication fails when TestHelpers included" do
    request.env["devise.mapping"] = Devise.mappings[:user]
    post :create, :user => {
      :email => "nosuchuser@example.com",
      :password => "wevdude"
    }
    assert_equal 200, @response.status
    assert_template "sessions/new"
  end
end
