require 'spec_helper'

describe SettingsController do
  describe "GET 'index'" do
    context "as guest" do
      it "should redirect to sign in" do
        get 'index'
        response.should redirect_to new_user_session_url
      end
    end
    
    context "as logged user" do
      before(:each) do
        @user = User.make!
        log_in @user
        get 'index'
      end
      
      it "should redner index" do
        response.should render_template 'index'
      end
      
      it 'assigns setting' do
        assigns(:setting)
      end
    end
  end
end
