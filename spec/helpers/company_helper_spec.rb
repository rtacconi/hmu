require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the BlogHelper. For example:
#
# describe BlogHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       helper.concat_strings("this","that").should == "this that"
#     end
#   end
# end
describe CompanyHelper do
  describe "email_link" do
    it "returns a link" do
      helper.email_link(User.make!.company).should =~ /<a href/
    end
    
    it "returns missing user if company.user is nil" do
      helper.email_link(Company.make!).should == 'missing user'
    end
  end
end
