require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the DashboardHelper. For example:
#
# describe DashboardHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       helper.concat_strings("this","that").should == "this that"
#     end
#   end
# end
describe DashboardHelper do
  describe "render_company_logo" do
    it "should return the image if the image is not null" do
      company = Company.make!
      company.logo.should be_present
      content = helper.render_company_logo(company)
      content.should =~ /image.jpg/
    end
    
    it "should return nil if the image is is null" do
      company = Company.make! :logo_file_name => nil
      content = helper.render_company_logo(company)
      content.should == nil
    end
  end
end
