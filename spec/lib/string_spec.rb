require 'spec_helper'

# describing this file /config/initializers/extensions/string.rb

describe String do
  it "counts the words in a string" do
    "My name is Riccardo".words.should == 4
  end
end