require "spec_helper"

describe Notifier do
  describe "welcome" do
    let(:mail) { Notifier.welcome(User.make! :email => 'to@example.org') }

    it "renders the headers" do
      mail.subject.should eq("Welcome")
      mail.to.should eq(["to@example.org"])
      mail.from.should eq(["no-reply@hitmeup.co"])
    end

    it "renders the body" do
      mail.body.encoded.should match("Hi")
    end
  end

end
