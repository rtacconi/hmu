require 'spec_helper'

describe Company do
  describe "profile completed" do
    it "returns true if :name, :your_name, :phone, :post_code, 
        :address_line1 and :logo are present" do
      Company.make!(:name => 'Acme inc', 
                    :your_name => 'Richard Moore', 
                    :phone => '01 2233444', 
                    :post_code => 'EC2 E33', 
                    :address_line1 => '34 John Street', 
                    :logo => get_image).should be_profile_completed
    end
    
    it "returns false if any of :name, :your_name, :phone, :post_code, 
        :address_line1 is blank" do
      Company.make!(:name => 'Acme inc', 
                    :your_name => 'Richard Moore', 
                    :phone => '01 2233444', 
                    :post_code => '', # it is blank! 
                    :address_line1 => '34 John Street').should_not be_profile_completed
    end
    
    it "has confirmation_state set to :profile_completed when the profile info are filled" do
      Company.make!(:name => 'Acme inc', 
                    :your_name => 'Richard Moore', 
                    :phone => '01 2233444', 
                    :post_code => 'EC2 E33', 
                    :address_line1 => '34 John Street', 
                    :logo => get_image).confirmation_state == 'profile_completed'
    end
  end
  
  describe "design completed" do
    it "returns true if :background, :font_colour and :deal_logo are present" do
      Company.make!(:name => 'Acme inc',
                    :your_name => 'Richard Moore', 
                    :phone => '01 2233444', 
                    :post_code => 'EC2 E33', 
                    :address_line1 => '34 John Street',
                    :deal_logo_file_name => 'image.jpg',
                    :deal_logo_content_type => 'image/jpeg',
                    :deal_logo_file_size => '200',
                    :background_file_name => 'image.jpg',
                    :background_content_type => 'image/jpeg',
                    :background_file_size => '200',
                    :font_colour => '#ad4c7b').should be_design_completed
    end
    
    it "has design_completed state when design is completed" do
      Company.make!(:name => 'Acme inc',
                    :your_name => 'Richard Moore', 
                    :phone => '01 2233444', 
                    :post_code => 'EC2 E33', 
                    :address_line1 => '34 John Street',
                    :deal_logo_file_name => 'image.jpg',
                    :deal_logo_content_type => 'image/jpeg',
                    :deal_logo_file_size => '200',
                    :background_file_name => 'image.jpg',
                    :background_content_type => 'image/jpeg',
                    :background_file_size => '200',
                    :font_colour => '#ad4c7b').confirmation_state == 'design_completed'
    end
  end
  
  describe "bank details completed" do
    it "returns true if bank_name, branch, sort_code, account_number are present" do
      c = Company.make!(:name => 'Acme inc',
                    :your_name => 'Richard Moore', 
                    :phone => '01 2233444', 
                    :post_code => 'EC2 E33', 
                    :address_line1 => '34 John Street',
                    :deal_logo_file_name => 'image.jpg',
                    :deal_logo_content_type => 'image/jpeg',
                    :deal_logo_file_size => '200',
                    :background_file_name => 'image.jpg',
                    :background_content_type => 'image/jpeg',
                    :background_file_size => '200',
                    :font_colour => '#ad4c7b')
      c.setting.bank_name = "A bank"
      c.setting.branch = 'Stratform Upon Avon'
      c.setting.sort_code = '444444'
      c.setting.account_number = '084039089234'
      c.setting.save!
      c.should be_bank_details_present
    end
    
    it "sets confirmation_state to details_completed if current state is design_completed" do
      c = Company.create!(:name => 'Acme inc',
                    :your_name => 'Richard Moore',
                    :phone => '01 2233444',
                    :post_code => 'EC2 E33',
                    :address_line1 => '34 John Street',
                    :deal_logo_file_name => 'image.jpg',
                    :deal_logo_content_type => 'image/jpeg',
                    :deal_logo_file_size => '200',
                    :background_file_name => 'image.jpg',
                    :background_content_type => 'image/jpeg',
                    :background_file_size => '200',
                    :font_colour => '#ad4c7b')
      c.confirmation.trigger(:not_completed)
      c.confirmation.trigger(:profile_completed)
      c.confirmation.trigger(:design_completed)
      c.save!
      c.reload # load side effects
      c.setting.update_attributes(:bank_name => "A bank",
                                  :branch => 'Stratform Upon Avon',
                                  :sort_code => '444444',
                                  :account_number => '084039089234')
      c.save!                          
      c.reload # load side effects
      c.confirmation_state.should == 'details_completed'
    end
  end
  
  it 'creates setting' do
    c = Company.create!(:name => 'Acme inc', 
                  :your_name => 'Richard Moore', 
                  :phone => '01 2233444', 
                  :post_code => 'EC2 E33', 
                  :address_line1 => '34 John Street')
    c.setting.should be_present
    c.confirmation_state.should == 'not_completed' # he has to complete the profile
  end
  
  describe "address" do
    it "joins the fields and avoid nil error" do
      @user = User.make!
      @user.company.update_attributes(city: nil, address_line2: nil, post_code: nil)
      @user.company.address.should be_present
    end
  end
end
