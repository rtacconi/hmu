require 'spec_helper'

describe Deal do
  
  context "on creation" do
    it "should set public to true" do
      user = User.make!
      deal = Deal.make! :company => user.company
      deal.published.should == true
    end
  end
  
  describe "remaining items" do
    it "should return 9 if quantity is 10 and 1 is taken" do
      user = User.make!
      deal =  Deal.make! :blocked => false, 
                         :quantity => 10, 
                         :company => user.company
      payment =  Payment.make! :deal_id => deal.id,
                               :token => 'e7e7e7w3hhh',
                               :status => :paid
      deal.remaining.should == 9
    end
  end
  
  describe "remaining time" do
    it "should return the number of remaining days, hours, minutes, seconds" do
      user = User.make!
      Timecop.freeze(Time.parse('2010-12-09 10:10:20')) do
        @deal = Deal.make! :start_time => Time.parse('2010-12-10 12:30:20'), 
                           :end_time => Time.parse('2010-12-13 02:30:30'),
                           :company => user.company
        Timecop.travel(Time.parse('2010-12-11 22:30:30'))
        @deal.due_days.should == 1
        @deal.due_hours.should == 4
        @deal.due_minutes.should == 0
        @deal.due_seconds.should == 0
      end
    end
  end
  
  describe "past deals" do
    it "should find old deals" do
      Company.delete_all
      Deal.delete_all
      User.delete_all
      @user = User.make!
      Timecop.freeze(10.days.ago) do
        Deal.make! :company => @user.company
      end
      Timecop.freeze(10.days.since) do
        Deal.past(@user.company).count.should == 1
      end
    end
  end

  describe "current deals" do
    it "should find current and future deals" do
      Company.delete_all
      Deal.delete_all
      User.delete_all
      @user = User.make!
      Timecop.freeze(10.days.ago) do
        Deal.make! :company => @user.company
      end
      
      Deal.make! :company => @user.company
      Deal.current(@user.company).count.should == 1
    end
  end
  
  describe "discount" do
    before(:each) do
      Company.delete_all
      Deal.delete_all
      User.delete_all
      @user = User.make!
    end
    
    it "displays 50 if RRP is 1000 and price is 500" do
      Deal.make!(:rrp => 1000, 
                 :price => 500,
                 :company => @user.company).discount.to_i.should == 50 # 50%
    end
    
    it "displays 20 if RRP is 1000 and price is 800" do
      Deal.make!(:rrp => 1000, 
                 :price => 800,
                 :company => @user.company).discount.to_i.should == 20 # 20%
    end
    
    it "displays 90 if RRP is 1000 and price is 100" do
      Deal.make!(:rrp => 1000, 
                 :price => 100,
                 :company => @user.company).discount.to_i.should == 90 # 90%
    end
    
    describe "geocoding" do
      it "sets lat and lng (before create and update)" do
        user = User.make!
        deal = Deal.create!(:latitude => nil,
                            :longitude => nil,
                            :company => user.company,
                            :title => "Deal 1",
                            :description => 'Some text',
                            :small_print => 'Terms',
                            :price => 10.00,
                            :rrp => 12.00,
                            :quantity => 33,
                            :min_quantity => 3,
                            :post_code => 'CV31 1EH',
                            :start_time => 1.day.since,
                            :end_time => 2.days.since)
        deal.latitude.should be_present
        deal.longitude.should be_present
      end
    end
  end
  
  it "loads the payments" do
    user = User.make!
    deal = Deal.make! :company => user.company
    voucher = VoucherCode.make! :deal => @deal
    voucher.payment.update_attribute(:deal_id, deal.id)
    deal.payments.count.should == 1
  end
  
  describe "a blocked object" do
    it "should be blockable" do
      user = User.make!
      deal = Deal.make! :blocked => false, :company => user.company
      deal.block!
      deal.should be_blocked
    end
  end
  
  describe "on creation" do
    it "sets blocked to false or nil" do
      user = User.make!
      deal = Deal.make! :company => user.company
      deal.blocked.should be_false
    end
  end
  
  describe "after creation" do
    it "should send an email to the seller" do
      user = User.make!
      lambda {
        Deal.make! :company => user.company
      }.should change(ActionMailer::Base.deliveries, :count).by(2)
    end
    
    it "generates code to redeem the vouchers" do
      Delayed::Job.should_receive(:enqueue)
      user = User.make!
      Deal.make! :quantity => 2, :company => user.company
    end
  end
  
  describe "validation" do
    it "should not save if min qt is greater than max qt" do
      user = User.make!
      deal = Deal.make  :quantity => 2, 
                        :min_quantity => 3, 
                        :company => user.company
      deal.should_not be_valid
    end
    
    it "should not validate end time if it is before start time" do
      user = User.make!
      deal = Deal.make  :start_time => 1.day.since, 
                        :end_time => Time.now, 
                        :company => user.company
      deal.should_not be_valid
    end
    
    it "should not validate if start time is in the past" do
      user = User.make!
      deal = Deal.make :start_time => 1.day.ago, 
                       :end_time => 1.day.since, 
                       :company => user.company
      deal.start_time = 1.day.ago
      deal.should_not be_valid
    end
    
    it "should not validate if time range is more than 3 days" do
      
    end
  end
  
  describe "expiry time" do
    it "adds 2 months from end_time" do
      user = User.make!
      deal = Deal.make! :start_time => 1.day.since, 
                        :end_time => 2.days.since, 
                        :company => user.company
      deal.expiry_time.should > 2.months.since # 2 months + 2 days since
    end
  end
  
  describe "tipped?" do
    it "returns true if all payments paid are greater then or equal to the quantity" do
      user = User.make!
      deal = Deal.make! :start_time => 1.day.since, 
                        :end_time => 2.days.since, 
                        :company => user.company,
                        :quantity => 2,
                        :min_quantity => 2
      Payment.make! :deal_id => deal.id, :token => 'jas9s7f9d7fasohasof'
      Payment.make! :deal_id => deal.id, :token => 'jas9s7f9d7fasohasof'
      
      Payment.update_all("status = 'paid'", :deal_id => deal.id)
      Timecop.freeze(1.month.since) do
        deal.should be_tipped
      end
    end
    
    it "returns false if payments count is equal to quantity, but one payment is failed" do
      user = User.make!
      deal = Deal.make! :start_time => 1.day.since, 
                        :end_time => 2.days.since, 
                        :company => user.company,
                        :quantity => 2,
                        :min_quantity => 2
      Payment.make! :deal_id => deal.id, :status => :paid
      Payment.make! :deal_id => deal.id, :status => :failed
      
      Timecop.freeze(1.month.since) do
        deal.should_not be_tipped
      end
    end
  end
  
  describe "not_blocked" do
    before(:each) do
      @user = User.make!
    end
    
    it "returns deals with blocked set to false" do
      Deal.make! :blocked => false, :company => @user.company
      Deal.not_blocked.count.should == 1
    end
    
    it "returns deals with blocked set to nil" do
      Deal.make! :blocked => nil, :company => @user.company
      Deal.not_blocked.count.should == 1
    end
    
    it "should not return deals with blocked set to true" do
      Deal.make! :blocked => true, :company => @user.company
      Deal.not_blocked.count.should == 0
    end
  end
  
  describe "not_expired? and expired?" do
    it "returns true if the deal is not expired" do
      @user = User.make!
      deal = Deal.make! :company => @user.company, :expiry_time => 10.days.since
      deal.should be_not_expired
    end
  end
end
