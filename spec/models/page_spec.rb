require 'spec_helper'

describe Page do
  describe "blog" do
    it "finds the posts" do
      section = Section.create!(id: 1, :name => 'blog')
      page = Page.make! section_id: 1 # 1 == blog
      Page.find_posts.count.should == 1
    end
  end
  
  describe "published" do
    it "finds only published pages" do
      page = Page.make! published: true      
      Page.published.count.should == 1
    end
  end
end
