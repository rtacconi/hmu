require 'spec_helper'

describe Payment do
  describe "process payments" do
    context "with status :paid" do
      before(:each) do
        Payment.delete_all
        Deal.delete_all
        VoucherCode.delete_all
        user = User.make!
        Timecop.freeze(3.days.ago)
        @deal =  Deal.make! :min_quantity => 0,
                            :company => user.company
        Timecop.return
      end
    end
    
    context "with status :pending" do
      before(:each) do
        Payment.delete_all
        Deal.delete_all
        VoucherCode.delete_all
        user = User.make!
        Timecop.freeze(3.days.ago)
        @deal =  Deal.make! :min_quantity => 0,
                            :company => user.company
        Timecop.return
        
        @payment = Payment.make! :deal_id => @deal.id, :status => :pending
      end
      
      it "should delivery if the min quantity is set to 0" do
        Payment.should_receive(:delivery).once
        Payment.process([@deal])
      end
      
      it "should delivery if code is 200" do
        pending
        user = User.make!
        deal = Deal.make! :min_quantity => 0, 
                          :end_time => 3.days.since,
                          :company => user.company
        payment  =  Payment.make!(:deal => deal, :status => :pending)
        deal.payments.first.status == :pending
        SpreedlyCore.should_receive(:capture).and_return(stub(HTTParty::Response, :code => 200))
        payment.should_receive(:update_attributes).with(:status => :pending)
        VoucherCode.should_receive(:delivery)
        Payment.delivery(deal)
      end
      
      it "should process a payment if pending" do
        pending
        VoucherCode.should_receive(:delivery)
        @payment.should_receive(:update_attributes).with(:status => :paid)
        response = SpreedlyCore.stub!(:capture).and_return(stub(HTTParty::Response, :code => 200))
        Payment.process([@deal])
      end
    end
    
    context "with status :failed" do
      it 'should block a deal if a payment has failed but the others met the minimum quantity' do
        pending "check if it is still required"
        Payment.delete_all
        Deal.delete_all
        VoucherCode.delete_all
        user = User.make!
        
        Timecop.freeze(4.days.ago)
        @deal =  Deal.make! :min_quantity => 0,
                            :company => user.company
        Timecop.return

        @payment =  Payment.make! :deal_id => @deal.id, :status => :paid
        Payment.make! :deal_id => @deal.id, :status => :failed
      end
    end
  end
end
