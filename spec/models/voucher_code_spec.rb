require 'spec_helper'

describe VoucherCode do
  before(:each) { Delayed::Worker.delay_jobs = false }
  
  describe "generate" do
    it "should create 2 records if quantity is 2" do
      user = User.make!
      lambda {
        Deal.make! :quantity => 2, :company => user.company
      }.should change(VoucherCode, :count).by(2)
    end
  end
  
  describe "get_from_code" do
    it "returns the voucher if it exists, the company ok" do
      user = User.make!
      Deal.make! :quantity => 2, :company => user.company
      VoucherCode.get_from_code(user.company, VoucherCode.last.code).should be_true
    end
    
    it "does not return a voucher if the company is wrong" do
      user = User.make!
      user2 = User.make!
      Deal.make! :quantity => 2, :company => user2.company # wrong company and user
      VoucherCode.get_from_code(user.company, VoucherCode.last.code).should be_false
    end
    
    it "does not return expired vouchers" do
      user = User.make!
      deal = Deal.make! :quantity => 2, :company => user.company
      deal.update_attribute :expiry_time, 2.months.ago
      (deal.expiry_time < Time.now).should be_true
      VoucherCode.get_from_code(user.company, VoucherCode.last.code).should be_false
    end
  end
end
