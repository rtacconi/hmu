require 'support/blueprints'

module AcceptanceTest
  def self.included(klass)
    klass.class_eval do
      before :all do
        create_pages
      end
    end
  end

  protected
  # helpers methods
  def login_as_seller
    @user = User.make!
    visit user_session_path
    fill_in 'sign_in_user_email', :with => @user.email
    fill_in 'sign_in_user_password', :with => @user.password
    click_button 'Sign in'
  end
  
  def login_as_admin
    @user = User.make!(:admin => true)
    visit user_session_path
    fill_in 'sign_in_user_email', :with => @user.email
    fill_in 'sign_in_user_password', :with => @user.password
    click_button 'Sign in'
  end
  
  def login_as(who)
    visit user_session_path
    fill_in 'sign_in_user_email', :with => who.email
    fill_in 'sign_in_user_password', :with => who.password
    click_button 'Sign in'
  end
  
  def create_past_deal
    Timecop.freeze(Timecop.freeze(2.months.ago)) do
      user = User.make!
      Deal.make!(:company => user.company,:end_time => 1.month.since) # end_time is 2.months.ago
    end
  end
  
  def create_current_deal
    Timecop.freeze(Timecop.freeze(2.days.ago)) do
      user = User.make!(email: "#{rand(0..30000000000)}@example.com")
      Deal.make!(:company => user.company,:end_time => 1.month.since) # end_time is 2.months.ago
    end
  end
  
  def create_deal_and_log_in
    @user = User.make!(email: "#{rand(0..30000000000)}@example.com")
    Timecop.travel(1.day.ago)
    @deal = Deal.make!(:company => @user.company, end_time: 1.month.since)
    Timecop.travel(1.day.since)
    visit user_session_path
    fill_in 'sign_in_user_email', :with => @user.email
    fill_in 'sign_in_user_password', :with => @user.password
    click_button 'Sign in'
  end
  
  def random_number
    rand(100000000 + 1)
  end

  def random_email
    "#{random_number}@example.com"
  end
end