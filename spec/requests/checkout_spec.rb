require 'spec_helper'
require File.join File.dirname(__FILE__), "acceptance_test"

feature "Checkout process" do
  include AcceptanceTest
  
  scenario %q{The payment is created (Status created)
    Given I am on the checkout page
    When the system send the payment to gocardless
      And the payment is created
    Then I should see "The payment has been created. 
      We will send you an email with the voucher when the payment will be completed."
  } do
    pending
  end
  
  scenario %q{The payment fails (status failed)
    Given I am on the checkout page
    When the system send the payment to gocardless
      And the payment fails
    Then I should see "The payment failed. Please"
  } do
    pending
  end
end