require 'spec_helper'
require File.join File.dirname(__FILE__), "acceptance_test"

feature "Manage Seller profile" do
  include AcceptanceTest
  
  scenario %q{"A seller has set his profile and now has to set the design"
    Given I am a Seller
    When I am on the Desing page
    And I select an image for the company logo
    And I select an image for the deals background
    And I select a font for the links
    And I click Save Changes
    And I should be redirected to my Seller Dashboard
  } do
    pending
    @user = User.make!
    @user.company.update_attribute(:background_file_name, nil)
    visit user_session_path
    fill_in 'sign_in_user_email', :with => @user.email
    fill_in 'sign_in_user_password', :with => @user.password
    click_button 'Sign in'
    
    current_path.should == design_path
    page.attach_file('company_logo', File.join(Rails.root, "/spec/support/alex.png"))
    page.attach_file('company_background', File.join(Rails.root, "/spec/support/alex.png"))
    fill_in 'company[font_colour]', '#c74848'
    click_button 'SAVE CHANGES'
    current_path.should == dashboard_path
  end
end