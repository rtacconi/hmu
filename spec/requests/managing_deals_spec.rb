require 'spec_helper'
require File.join File.dirname(__FILE__), "acceptance_test"

feature "Managing deals" do
  include AcceptanceTest
  
  scenario %q{"I want to create a deal",
    Given I am logged in as a seller
    And I am on the Create a deal page
    And I fill in the title / headline 'Fantastic Website Deal'
    And I attach the deal picture
    And I fill in the description '20% discounts on all websites'
    And I fill in the small print 'Excludes e-commerce websites.'
    And I fill in the deal price '6,400'
    And I fill in the RRP '8,000'
    And I fill in the maximum sales '20'
    And I fill in the minimum sales '5'
    And I fill in the start date / time '14th December 09:00' [this uses a jQuery pop up]
    And I fill in the end date / time '14th February 00:00' [this uses a jQuery pop up]
    And I tick 'Show google map on deal page?' under 'Localisation'
    And I enter 'E1 6BJ' in the 'Postcode search field'
    The map should move to that location with a marker
    Then I click 'Create Deal'
    Then I am directed to the newly created Deal page
    And I see the message 'You may edit your deal until it goes live.'
  }, :js => true, :driver => :selenium do
    login_as_seller
    deals = Deal.count
    visit new_deal_path
    fill_in 'Title', :with => 'Fantastic Website Deal'
    attach_file('deal_photo', File.join(Rails.root, "/spec/support/alex.png"))
    fill_in 'Description', :with => '20% discounts on all websites'
    fill_in 'How to Redeem *', :with => 'text...'
    fill_in 'Small print', :with => 'Excludes e-commerce websites.'
    fill_in 'Deal price *', :with => '6400'
    fill_in 'RRP (i.e. Usual price)*', :with => '8000'
    fill_in 'Maximum sales *', :with => '20'
    fill_in 'Minimum sales *', :with => '5'
    fill_in 'Start date / time: *', :with => 1.day.since
    fill_in 'End date / time: *', :with => 2.days.since
    check 'Show google map on deal page?'
    fill_in 'Post code', :with => 'E1 6BJ'
    fill_in 'Street', :with => '34 John Street'
    click_button 'Create Deal'
    page.has_content?('You may edit your deal until it goes live.')
    Deal.count.should == 1
    current_path.should == deal_path(Deal.last)
    page.has_content?('You may edit your deal until it goes live.')
  end
  
  scenario %q{"I want to create a deal but validation fails",
    Given I am logged in as a seller
    And I am on the Create a deal page
    And I fill in the title / headline 'Fantastic Website Deal'
    And I do not fill the other required fields
    Then I click 'Create Deal'
    Then I should stay in the same page
    And I see 'Please fill all required fields'
  } do
    login_as_seller
    visit new_deal_path
    fill_in 'Title', :with => 'Fantastic Website Deal'
    click_button 'Create Deal'
    current_path.should == deals_path # same page
    page.has_content?("can't be blank")
  end
  
  scenario %q{"I want to edit a deal",
    Given I am logged in as a seller
    And I am on the Edit a deal page
    And I fill in the title / headline 'Fantastic Website Deal'
    And I attach the deal picture
    And I fill in the description '20% discounts on all websites'
    And I fill in the small print 'Excludes e-commerce websites.'
    And I fill in the deal price '6,400'
    And I fill in the RRP '8,000'
    And I fill in the maximum sales '20'
    And I fill in the minimum sales '5'
    And I tick 'Show google map on deal page?' under 'Localisation'
    And I enter 'E1 6BJ' in the 'Postcode search field'
    The map should move to that location with a marker
    Then I click 'Change Deal'
    Then I am directed to the updated Deal page
    And I see the message 'Your deal was successfully updated.'
  }, :js => true do
    user = User.make!
    Deal.make!(:company => user.company, :start_time => 1.day.since, :end_time => 3.days.since)
    login_as(user)
    visit edit_deal_path(Deal.last)
    fill_in 'Title', :with => 'Fantastic Website Deal'
    attach_file('deal_photo', File.join(Rails.root, "/spec/support/alex.png"))
    fill_in 'Description', :with => '20% discounts on all websites'
    fill_in 'How to Redeem *', :with => 'text...'
    fill_in 'Small print', :with => 'Excludes e-commerce websites.'
    fill_in 'Deal price *', :with => '6400'
    fill_in 'RRP (i.e. Usual price)*', :with => '8000'
    fill_in 'Maximum sales *', :with => '20'
    fill_in 'Minimum sales *', :with => '5'
    check 'Show google map on deal page?'
    fill_in 'Post code', :with => 'E1 6BJ'
    fill_in 'Street', :with => '34 John Street'
    click_button 'Change Deal'
    current_path.should == deal_path(Deal.last)
    page.has_content?('You may edit your deal until it goes live.')
  end
end