require 'spec_helper'
require File.join File.dirname(__FILE__), "acceptance_test"

feature "Pages" do
  include AcceptanceTest
  
  scenario %q{"An admin creates a page"
    As admin
    Given I am in the page listing
    And I click new page
    And I fill title with 'A Title'
    And summary with 'A Summary'
    And body with 'A body...'
    And I check 'Published'
    And I click 'Save'
    Then I should see the page
  }, :js => true, :driver => :selenium do
    pending "cannot fill_in a div with capybara... yet"
    create_pages
    login_as_admin
    visit pages_path
    click_link 'New Page'
    fill_in 'page_title', :with => 'A Title'
    fill_in 'page_summary', :with => 'A Summary'
    fill_in 'page_content', :with => 'A Body...'
    click_on 'Save'
    
    current_path.should == page_path(Page.last)
  end
  
  scenario %q{"A guess access a page"
    As a guest
    Given I am in the home page
    And I click 'Privacy Policy'
    Then I should in in 'Privacy Police' page 
  } do
    create_pages
    visit root_path
    click_link 'Privacy Policy'
    page.should have_content 'Privacy Policy'
  end
end

