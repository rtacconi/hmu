require 'spec_helper'
require File.join File.dirname(__FILE__), "acceptance_test"

feature "Seller dashboard" do
  include AcceptanceTest
  
  scenario %q{I view my dashboard
    Given I am logged in as a seller
    And I am viewing my dashboard
    I should see my company profile on the left with a link to edit
  } do
    login_as_seller
    current_path.should == dashboard_path
    page.has_content?('Company Profile')
    page.has_content?(@user.company.name)
    page.has_content?(@user.company.website)
    page.has_content?(@user.company.twitter)
    page.has_content?(@user.company.facebook)
  end
  
  scenario %q{I have no existing deals and view the dashboard
    Given I am logged in as a seller
    And I am viewing my dashboard
    And I have no deals
    On the right I should see "You don't have any deals setup at the moment", 
      then the button to 'Create a deal' underneath
  } do
    login_as_seller
    current_path.should == dashboard_path
    page.has_content?("You don't have any deals setup at the moment")
  end
  
  scenario %q{I have past deals and view the dashboard
    Given I am logged in as a seller
    And I am viewing my dashboard
    And I have deals that have ended
    I should see a list of past deals beneath the heading 'Past Deals'
  } do
    login_as_seller
    current_path.should == dashboard_path
    create_past_deal
    page.has_content?(Deal.first.title)
    page.has_content?("Expired")
  end
  
  scenario %q{I have current or future deals and view the dashboard
    Given I am logged in as a seller
    And I am viewing my dashboard
    And I have deals that have ended
    I should see a list of past deals beneath the heading 'Current & Future Deals'
  } do
    login_as_seller
    current_path.should == dashboard_path
    create_current_deal
    page.has_content?(Deal.first.title)
    page.has_content?("Deal 2222222expires")
  end
end