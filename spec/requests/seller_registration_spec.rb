require 'spec_helper'
require File.join File.dirname(__FILE__), "acceptance_test"

feature "Seller registration" do
  include AcceptanceTest
  
  scenario %q{"Sign up workflow", 
    Given I am a Guest
    When I am in the Create an account/Sign In page
    And I fill in all required fields
    And I click 'Create Account'
    Then I should be redirected to the profile page
    And I should see the flash message "Enter your profile details. Fill in some more information about your company to get started."
    And I fill all the required fields
    And I click Save Profile
    Then I should be redirected to the design page
    And I should see the flash message "Now customise how your deals appear. Make your deals match your company branding by adding your logo and company colours here."
    And I fill the required fields
    Then I should be redirected to the seller's dashbord
    And I should see the flash message "Now create a deal!"
    } do
    visit user_session_path
    fill_in 'user_company_attributes_name', :with => "We Make Websites Ltd"
    fill_in 'user_company_attributes_your_name', :with => "Alex..."
    fill_in 'user_email', :with => 'alex@wemakewebsites.com'
    fill_in 'user_company_attributes_phone', :with => '07752 715590'
    fill_in 'user_password', :with => 'password1'
    fill_in 'user_password_confirmation', :with => 'password1'
    click_button 'Create account'
    current_path.should == profile_path
    page.should have_content("Enter your profile details")
    fill_in "Your website", :with => 'virtuelogic.net'
    fill_in "Your facebook page URL", :with => 'rtacconi'
    fill_in "Company description", :with => "some text"
    attach_file('company_logo', File.join(Rails.root, "/spec/support/alex.png"))
    fill_in "Address line 1 *", :with => "34 John street"
    fill_in "City", :with => "London"
    fill_in "Post code *", :with => "EC2 3EE"
    click_on "Save Profile"
    current_path.should == design_path
    page.should have_content("Now customise how your deals appear")
    attach_file('company_deal_logo', File.join(Rails.root, "/spec/support/alex.png"))
    attach_file('company_background', File.join(Rails.root, "/spec/support/alex.png"))
    fill_in 'company_font_colour', :with => '#ad4c7b'
    click_on "Save Changes"
    current_path.should == dashboard_path
    page.should have_content("Now create a deal!")
  end
  
  scenario %q{"A Seller put a mismatching password",
    Given I am a Guest
    When I am in the Create an account/Sign In page
    And I fill in company name with "We Make Websites Ltd"
    And I fill in your name with "Alex O'Byrne"
    And I fill in email with "alex@wemakewebsites.com"
    And I fill in phone with "07752 715590"
    And I fill in password name with "password1"
    And I fill in confirm password name with "wrond_password"
    And I click 'Create Account'
    Then I should be redirected to the Create an account/Sign In page
    And I should see "Password doesn't match confirmation"
    } do
      visit user_session_path
      fill_in 'user_company_attributes_name', :with => "We Make Websites Ltd"
      fill_in 'user_company_attributes_your_name', :with => "Alex..."
      fill_in 'user_email', :with => 'alex@wemakewebsites.com'
      fill_in 'user_company_attributes_phone', :with => '07752 715590'
      fill_in 'user_password', :with => 'password1'
      fill_in 'user_password_confirmation', :with => 'different_password'
      click_button 'Create account'
      current_path.should == user_registration_path
      page.should have_content("Password *doesn't match confirmation")
  end
end