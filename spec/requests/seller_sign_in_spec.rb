require 'spec_helper'
require File.join File.dirname(__FILE__), "acceptance_test"

feature "Logins" do
  include AcceptanceTest
  
  background do
    @user = User.make!
    @user.company.setting.update_attributes(:bank_name => "A bank",
                                            :branch => 'Stratform Upon Avon',
                                            :sort_code => '444444',
                                            :account_number => '084039089234')
  end
  
  scenario %q{Successfully login as Seller", 
    Given I am a Guest  (In all cases that a guest is a seller)
    When I am in the login page
    And I fill in my email
    And I fill in my password
    And I click the Sign in button
    Then I should be redirected to Seller dashboard
  } do
    visit user_session_path
    fill_in 'sign_in_user_email', :with => @user.email
    fill_in 'sign_in_user_password', :with => @user.password
    click_button 'Sign in'
    current_path.should == dashboard_path
  end
  
  scenario %q{"Seller login fails", 
    Given I am a Guest
    When I am in the home page
    And I click the sign in button
    And I go to the Create an account/Sign In page
    And I fill in my email
    And I fill in with a wrong password
    And I click the Sign In button
    Then I should stay in the same page
    And I should see message 'Invalid email or password' and a prompt to retry, 
      as well as a link to 'Forgot your password?'
  } do
    visit home_index_path
    click_link 'Sign in'
    current_path.should == user_session_path
    fill_in 'user_email', :with => @user.email
    fill_in 'user_password', :with => 'wrong password'
    click_button 'Sign in'
    current_path.should == user_session_path # same page
    page.has_content?('Invalid email or password')
    find_link('Forgot your password?').visible?
  end
end


