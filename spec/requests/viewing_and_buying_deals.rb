require 'spec_helper'
require File.join File.dirname(__FILE__), "acceptance_test"

feature "Viewing and buying deals" do
  include AcceptanceTest
  scenario %q{I am on a deal page
          Given I am a guest or buyer
          And I am on a deal page
          I should see the deal information as per wireframe
          And a 'Buy it now' button should be visible if the number of deal purchases is below 'Maximum sales' or within time
          If I click on 'Buy it now' and it is active
          I should be directed to the checkout page
  } do
    pending
  end
  
  scenario %q{I am on a deal page [onsite e.g. wemakewebsites.hitmeup.co/20percentoff]
    Given I am the Seller that created the deal
    And I am on a deal page
    I should see the deal information as per wireframe
    Except for the Buy it now button, which should be disabled/opacity 0.8
    And I should see a tab with links to the admin pages for this deal
  } do
    pending
  end
  
  scenario %q{I am on a deal page [embeddable banner]
    Given I am a guest or buyer
    And I see a banner on a seller's website
    I should see the deal information as per design [to be done]
    If I click on 'Buy it now'
    And should be directed to the checkout page on hitmeup.co
  } do
    pending
  end
  
  scenario %q{I am on a deal page [embeddable page]
    Given I am a guest or buyer
    And I see a page on a seller's website that includes the embeddable page view of a deal
    I should see the deal information as per design 
      [to be done, will show full deal details like the onsite deal page]
    And a 'Buy it now' button should be visible if the number of deal purchases 
      is below 'Maximum sales'
    If I click on 'Buy it now' and it is active
    I should be directed to the checkout page
    And should be directed to the checkout page on hitmeup.co
  } do
    pending
  end
  
  scenario %q{I am on a deal page [facebook]
    Given I am a guest or buyer
    And I see a deal on a facebook page
    I should see the deal information as per design [to be done]
    And a 'Buy it now' button should be visible if the number of deal purchases is below 'Maximum sales'
    If I click on 'Buy it now' and it is active
    I should be directed to the checkout page
    And should be directed to the checkout page on hitmeup.co
  } do
    pending
  end
  
  scenario %q{I am on the checkout
    Given I am a buyer with an account
    I should enter my email address
    And I enter a correct password
    If I click on 'Sign in'
    And the deal is above minimum and within time
    And should be directed to the checkout page on hitmeup.co
  } do
    pending
  end
end