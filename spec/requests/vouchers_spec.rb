require 'spec_helper'
require File.join File.dirname(__FILE__), "acceptance_test"

feature "Vouchers" do
  include AcceptanceTest
  
  background do
    create_deal_and_log_in
    @payment = Payment.make!(deal: @deal)
    @voucher = VoucherCode.make!(deal: @deal, payment: @payment)
  end
  
  scenario %q{A seller successfully redeems a voucher
    Given I am a seller
    When I visit the vouchers page
    Then I enter the voucher code
    And I click 'Redeem'
    Then I see 'This Voucher is to redeem the following deal: Deal Nane and is valid'
    And I click 'Click here to redeem it now.'
    Then I should see 'The voucher has now been used.' (Success)
  } do
    pending
    visit vouchers_deal_path(@deal)
    fill_in :code, with: @voucher.code
    click_button 'Redeem'
    page.has_content?('This Voucher is to redeem the following deal: Deal Nane and is valid')
    click_on 'Click here to redeem it now.'
    page.has_content?('The voucher has now been used.')
  end
  
  scenario %q{A seller enters a wrong voucher code
    Given I am a seller
    When I visit the vouchers page
    Then I enter a wrong voucher code
    And I click 'Redeem'
    Then I am redirected to the dashboard
  } do
    pending
    visit vouchers_deal_path(@deal)
    fill_in :code, with: 'EEDE44'
    click_button 'Redeem'
    current_path.should == dashboard_path
  end
  
  scenario %q{A seller tries to redeem an expired voucher
    Given I am a seller
    When I visit the vouchers page
    Then I enter the voucher code
    And I click 'Redeem'
    Then I see 'This voucher is expired! Expiry date is...'
  }, driver: :selenium do
    @user.company.should be_design_completed
    @deal.update_attributes(:expiry_time => 1.day.ago)
    visit vouchers_deal_path(@deal)
    fill_in :code, with: @voucher.code
    click_button 'Redeem'
    page.has_content?('This voucher is expired! Expiry date is')
  end
  
end