require "spec_helper"

describe Admin::AnalyticsController do
  describe "routing" do

    it "routes to #index" do
      get("/admin/analytics").should route_to("admin/analytics#index")
    end

    it "routes to #new" do
      get("/admin/analytics/new").should route_to("admin/analytics#new")
    end

    it "routes to #show" do
      get("/admin/analytics/1").should route_to("admin/analytics#show", :id => "1")
    end

    it "routes to #edit" do
      get("/admin/analytics/1/edit").should route_to("admin/analytics#edit", :id => "1")
    end

    it "routes to #create" do
      post("/admin/analytics").should route_to("admin/analytics#create")
    end

    it "routes to #update" do
      put("/admin/analytics/1").should route_to("admin/analytics#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/admin/analytics/1").should route_to("admin/analytics#destroy", :id => "1")
    end

  end
end
