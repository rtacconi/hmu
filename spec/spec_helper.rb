require 'spork'
require 'capybara/rspec'
require 'machinist'

Spork.prefork do  
  ENV["RAILS_ENV"] ||= 'test'
  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
  require 'rspec_spinner'
  
  Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

  RSpec.configure do |config|
    config.mock_with :rspec
    
    config.before(:each) do
      Timecop.return
      FakeWeb.clean_registry
    end
    
    config.include Devise::TestHelpers, :type => :controller

    config.use_transactional_fixtures = true
    
    config.before do
      Capybara.current_driver = Capybara.javascript_driver if example.metadata[:js]
      Capybara.current_driver = example.metadata[:driver] if example.metadata[:driver]
    end
    
    config.after do
      Capybara.use_default_driver
    end
    
    config.use_transactional_fixtures = false

    config.before(:suite) do
      DatabaseCleaner.strategy = :truncation
    end

    config.before(:each) do
      DatabaseCleaner.start
    end

    config.after(:each) do
      DatabaseCleaner.clean
    end
  end

  Capybara.javascript_driver = :webkit

  Capybara::Driver::Webkit::Browser.class_eval do 
    def forward_stdout(pipe); end;
  end
end

Spork.each_run do 
  Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f} 
  DatabaseCleaner.clean
  Dir[Rails.root.join("app/models/**/*.rb")].each {|f| load f}
end

def get_image
  @image = File.new(Rails.root.to_s+"/spec/support/alex.gif") if @image.nil?
  
  @image
end
