require 'machinist/active_record'

Company.blueprint do
  name { "Office Tronic#{rand(10 ** 10)}" }
  website { 'officetronic.com' }
  facebook { 'officetronic' }
  twitter { 'officetronic' }
  description { 'We are a company dealing with electronics and office supply' }
  address_line1 {'34 John Street'}
  address_line2 {''}
  post_code { 'EC2 3RR' }
  phone { '01 343243434' }
  city { 'London' }
  your_name { 'john Doe' }
  logo_file_name { 'image.jpg' }
  logo_content_type { 'image/jpeg' }
  logo_file_size { '200' }
  deal_logo_file_name { 'image.jpg' }
  deal_logo_content_type { 'image/jpeg' }
  deal_logo_file_size { '200' }
  background_file_name { 'image.jpg' }
  background_content_type { 'image/jpeg' }
  background_file_size { '200' }
  font_colour { '#ad4c7b' }
  confirmation_state { 'details_completed' }
end

Deal.blueprint do
  company {Company.make!}
  title { '50% off for web sites dev' }
  description { '50% of discount...' }
  quantity { 1 }
  min_quantity { 0 }
  start_time { 1.day.since }
  end_time { 2.days.since }
  expiry_time { 1.month.since }
  price { 10 }
  shipping_cost{ 100 } # in cents
  published { true }
  small_print { 'Some text' }
  show_map { true }
  post_code { 'E1 6BJ' }
  rrp {15}
  photo_file_name { 'test.png' }
  photo_content_type { 'image/png' }
  photo_file_size { 123 }
  photo_updated_at { Time.now }
end


DLog.blueprint do
  # Attributes here
end

Page.blueprint do
  title { 'A Test' }
  summary { 'A summary' }
  body { 'This is the body of the page' }
end

Payment.blueprint do
  transaction_date { Time.now }
  amount { 12.50 }
  status { 'paid' }
  first_name { 'Jon' }
  last_name { 'Doe' }
  email { 'jd@ex.com' }
  email_confirmation { 'jd@ex.com' }
  first_name { 'John' }
  last_name { 'Doe' }
  billing_address_line1 { '4 Regent Street' }
  post_code { 'CV31 1EH' }
  city { 'Leamington Spa' }
  country { 'GB' }
end

Section.blueprint do
  # Attributes here
end


SellerPayment.blueprint do
  # Attributes here
end

Setting.blueprint do
end

User.blueprint(:admin) do
  email { "admin#{sn}@example.com" }
  password { "password" }
  admin { true }
  company { Company.make! }
end

User.blueprint do 
  email { "user#{sn}@example.com" }
  password { "password" }
  company { Company.make! }
end

VoucherCode.blueprint do
  deal { Deal.make! }
  payment { Payment.make! }
  code { "6BPUU8L" }
end


