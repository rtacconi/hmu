def log_in(user)
  sign_in user
  controller.stub(:authenticate_user!).and_return true
  controller.stub(:authenticate_user?).and_return true
  controller.stub(:current_user).and_return user
end

def log_in_as_user
  @user = mock_model(User, :id => 100, :company => 1, :admin => false).as_null_object
  log_in @user
end

def log_in_as_admin
  company = Company.create!(
    :name => "Office Tronic#{rand(10 ** 10)}",
    :website => 'officetronic.com',
    :facebook => 'officetronic',
    :twitter => 'officetronic',
    :description => 'We are a company dealing with electronics and office supply',
    :address_line1 => '34 John Street',
    :address_line2 => '',
    :post_code => 'EC2 3RR',
    :phone => '01 343243434',
    :city => 'London',
    :your_name => 'john Doe',
    :font_colour => '000000',
    :logo => get_image,
    :background => get_image,
    :deal_logo => get_image)
  user = User.make!(:admin => true, :company => company)
  sign_in user
  controller.stub(:authenticate_user!).and_return true
  controller.stub(:authenticate_user?).and_return true
  controller.stub(:current_user).and_return user
end

def create_pages
  Page.delete_all
  Page.create!(:id => 1,
               :title => 'Terms & Conditions', 
               :summary => 'Please read carefully',
               :body => 'This web site...',
               :published => true,
               :permanent => true)

  Page.create!(:id => 1,
              :title => 'Privacy Policy', 
              :summary => 'How your privacy is protected',
              :body => 'We do not give your information to any...',
              :published => true,
              :permanent => true)
  # avoid to test static pages when testing Mercury editor
  Page.make!(:id => 4) # static page
  Page.make!(:id => 5) # static page
end

def get_admin_user
  user = User.where(:admin => true).first
  return User.make!(:admin) unless user
end